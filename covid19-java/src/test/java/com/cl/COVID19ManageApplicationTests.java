package com.cl;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class COVID19ManageApplicationTests {

    /**
     * Spring Security有两个重要概念：Authentication（认证）和 Authorization（授权）
     * <p>
     * Spring Security登录认证主要涉及两个重要的接口：UserDetailService 和 UserDetails 接口
     * <p>
     * UserDetailService 接口主要定义了一个方法 loadUserByUsername(String username)用于完成用户信息的查询
     * 其中 username就是登录时的登录名称，登录认证时，需要自定义一个实现类实现UserDetails接口，完成数据库查询，该接口返回UserDetail
     * <p>
     * UserDetail 主要用于封装认证成功时的用户信息，即UserDetailService 返回的用户信息，可以用spring自己的user对象，最好实现UserDetail接口，自定义用户对象。
     * <p>
     * Spring Security认证步骤：
     * 1. 自定义UserDetails类：当实体对象字段不满足时 需要自定义UserDetails，一般都要自定义UserDetails。
     * 2. 自定义UserDetailService类，主要用于从数据库查询用户信息。
     * 3. 创建登录认证成功处理器，认证成功后需要返回JSON数据，菜单权限等。
     * 4. 创建登录认证失败处理器，认证失败需要返回JSON数据，给前端判断。
     * 5. 创建匿名用户访问无权限资源时处理器，匿名用户访问时，需要提示JSON。
     * 6. 创建认证过的用户访问无权限资源时的处理器，无权限访问时，需要提示JSON。
     * 7. 配置Spring Security配置类，把上面自定义的处理器交给Spring Security。
     */

//    @Test
//    void contextLoads() {
//        String host = "https://ncovdata.market.alicloudapi.com";
//        String path = "/ncov/cityDiseaseInfoWithTrend";
//        String method = "GET";
//        String appcode = "9d81d3e771f247ae84d292aaaf06ca4a";
//        Map<String, String> headers = new HashMap<String, String>();
//        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
//        headers.put("Authorization", "APPCODE " + appcode);
//        Map<String, String> querys = new HashMap<String, String>();
//
//
//        try {
//            /**
//             * 重要提示如下:
//             * HttpUtils请从
//             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
//             * 下载
//             *
//             * 相应的依赖请参照
//             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
//             */
//            HttpResponse response = HttpUtil.doGet(host, path, method, headers, querys);
//            System.out.println(response.toString());
//            //获取response的body
//            System.out.println(EntityUtils.toString(response.getEntity()));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    void test1(){
//        System.out.println("111");
//        String url = "https://api.muxiaoguo.cn/api/epidemic?api_key=800605ce84aed771&type=epidemicHotspot";
//        String s = cn.hutool.http.HttpUtil.get(url);
//        System.out.println(s);
//    }

}
