package com.cl.utils;

import com.cl.entity.SysMenu;
import com.cl.entity.vo.RouterVo;
import org.springframework.beans.BeanUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 生成菜单树
 *
 * @author starsea
 * @date 2022-07-18 14:50
 */
public class MenuTree {

    /**
     * 生成路由
     *
     * @param meuList 菜单列表
     * @param pid     父菜单id
     * @return
     */
    public static List<RouterVo> makeRouter(List<SysMenu> meuList, Long pid) {
        List<RouterVo> routerList = new ArrayList<>();
        //如果menuList菜单列表不为空，则使用菜单列表，否则创建集合对象
        Optional.ofNullable(meuList).orElse(new ArrayList<SysMenu>())
                //筛选不为空的菜单及菜单父id相同的数据
                .stream().filter(item -> item != null && item.getParentId() == pid)
                .forEach(item -> {
                    //创建路由对象
                    RouterVo router = new RouterVo();
                    router.setName(item.getName());//路由名称
                    router.setPath(item.getPath());//路由地址
                    //判断是否是一级菜单
                    if (item.getParentId() == 0L) {
                        router.setComponent("Layout");//一级菜单组件
                        router.setAlwaysShow(true);//显示路由
                        router.setOrderNum(item.getOrderNum());
                    } else {
                        router.setComponent(item.getUrl());//具体的组件
                        router.setAlwaysShow(false);//折叠路由
                    }
                    //设置meta信息
                    router.setMeta(router.new Meta(item.getLabel(),
                            item.getIcon(),
                            item.getCode().split(",")));
                    //递归生成路由
                    List<RouterVo> children = makeRouter(meuList, item.getMenuId());
                    router.setChildren(children);//设置子路由到路由对象中
                    //将路由信息添加到集合中
                    routerList.add(router);
                });
        List<RouterVo> sortedRouter = routerList.stream().sorted(Comparator.comparing(RouterVo::getOrderNum)).collect(Collectors.toList());
        return sortedRouter;
    }

    /**
     * 生成菜单树
     *
     * @param meuList
     * @param pid
     * @return
     */
    public static List<SysMenu> makeMenuTree(List<SysMenu> meuList, Long pid) {

        //创建集合保存菜单
        List<SysMenu> permissionList = new ArrayList<SysMenu>();
        //如果menuList菜单列表不为空，则使用菜单列表，否则创建集合对象
        Optional.ofNullable(meuList).orElse(new ArrayList<>())
                .stream().filter(item -> item != null &&
                Objects.equals(item.getParentId(), pid))
                .forEach(item -> {
                    //创建菜单权限对象
                    SysMenu permission = new SysMenu();
                    //复制属性
                    BeanUtils.copyProperties(item, permission);
                    //获取每一个item的下级菜单,递归生成菜单树
                    List<SysMenu> children = makeMenuTree(meuList, item.getMenuId());
                    //设置子菜单
                    permission.setChildren(children);
                    //将菜单对象添加到集合
                    permissionList.add(permission);
                });
        return permissionList;
    }
}