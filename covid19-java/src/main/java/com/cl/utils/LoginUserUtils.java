package com.cl.utils;


import com.cl.security.domain.LoginUser;
import org.springframework.security.core.context.SecurityContextHolder;

public class LoginUserUtils {

    public LoginUserUtils(){

    }
    public static LoginUser getLoginUser(){
        LoginUser user = (LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user;
    }
}
