package com.cl.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

/**
 * post 请求分页入参
 *
 * @author starsea
 * @date 2022-10-31 11:48
 */
@Data
public class PageRequest<T> {

    private Page page;
    private T data;
}
