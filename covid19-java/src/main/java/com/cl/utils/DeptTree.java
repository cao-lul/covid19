package com.cl.utils;

import com.cl.entity.SysDept;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 生成部门树
 *
 * @author Starsea
 * @date 2022-07-23 17:39
 */
public class DeptTree {

    /**
     * 生成部门树
     * @param deptList
     * @param parentId
     * @return
     */
    public static List<SysDept> makeDeptTree(List<SysDept> deptList, Long parentId) {
        //用于保存部门信息
        List<SysDept> list = new ArrayList<>();
        //如果deptList部门列表不为空，则使用部门列表，否则创建集合对象
        Optional.ofNullable(deptList).orElse(new ArrayList<SysDept>())
                .stream().filter(item -> item != null && item.getParentId() == parentId)
                .forEach(item -> {
                    SysDept sysDept = new SysDept();
                    //复制属性
                    BeanUtils.copyProperties(item, sysDept);
                    //获取每个item下的子部门，递归生成树
                    List<SysDept> children = makeDeptTree(deptList, item.getDeptId());
                    //设置子部门
                    sysDept.setChildren(children);
                    //将部门对象添加到集合
                    list.add(sysDept);
                });
        return list;
    }
}
