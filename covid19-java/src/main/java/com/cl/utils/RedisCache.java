package com.cl.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Redis工具类
 *
 * @author starsea
 * @date 2022-07-14 13:42
 */
@SuppressWarnings(value = {"unchecked", "rawtypes"})
@Component
public class RedisCache {

    @Autowired
    public RedisTemplate redisTemplate;

    //存缓存
    public void set(String key, String value, Integer timeOut) {
        redisTemplate.opsForValue().set(key, value, timeOut, TimeUnit.SECONDS);
    }

    //取缓存
    public String get(String key) {
        return (String) redisTemplate.opsForValue().get(key);
    }

    //清除缓存
    public void del(String key) {
        redisTemplate.delete(key);
    }
}
