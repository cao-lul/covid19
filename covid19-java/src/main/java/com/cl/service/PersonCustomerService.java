package com.cl.service;

import com.cl.entity.PersonCustomer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【person_customer(来访登记表)】的数据库操作Service
* @createDate 2022-11-14 21:13:24
*/
public interface PersonCustomerService extends IService<PersonCustomer> {

}
