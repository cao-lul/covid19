package com.cl.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.VaccinationRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cl.entity.query.VaccinationRecordQuery;

import java.util.List;

/**
 * <p>
 * 疫苗接种记录 服务类
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
public interface VaccinationRecordService extends IService<VaccinationRecord> {

    List<VaccinationRecordQuery> queryList(QueryWrapper<VaccinationRecordQuery> wrapper);
}
