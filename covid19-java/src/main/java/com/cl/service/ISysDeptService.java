package com.cl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cl.entity.SysDept;

import java.util.List;

/**
* @author GH
* @description 针对表【sys_dept(部门表)】的数据库操作Service
* @createDate 2022-07-13 10:37:37
*/
public interface ISysDeptService extends IService<SysDept> {

    /**
     * 查询部门列表
     * @param sysDept
     * @return
     */
    List<SysDept> getDeptList(SysDept sysDept);

    /**
     * 查询上级部门列表
     * @return
     */
    List<SysDept> getParentDept();

    /**
     * 查询部门下是否存在子部门
     * @param deptId
     * @return
     */
    boolean selectChildrenDept(Long deptId);

    /**
     * 查询部门下是否存在用户
     * @param deptId
     * @return
     */
    boolean selectUserDept(Long deptId);
}
