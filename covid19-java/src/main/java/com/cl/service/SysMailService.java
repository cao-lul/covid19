package com.cl.service;

import com.cl.entity.SysMail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cl.entity.vo.MailVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 邮件服务表 服务类
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
public interface SysMailService extends IService<SysMail> {

    boolean upAndSave(List<MultipartFile> files, MailVo mailVoReal,String mailType);
}
