package com.cl.service;

import com.cl.entity.ClockingInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cl.entity.query.ClockingInfoQuery;
import com.cl.entity.vo.ClockInfoVo;

/**
 * <p>
 * 打卡信息表 服务类
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
public interface ClockingInfoService extends IService<ClockingInfo> {

    public ClockInfoVo infoReport(ClockingInfoQuery query) throws Exception;

}
