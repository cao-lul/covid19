package com.cl.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cl.entity.CovidPurchaseRecord;

/**
* @author Administrator
* @description 针对表【covid_purchase_record(菜单信息表)】的数据库操作Service
* @createDate 2022-11-12 14:25:02
*/
public interface CovidPurchaseRecordService extends IService<CovidPurchaseRecord> {

}
