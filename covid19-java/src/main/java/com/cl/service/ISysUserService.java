package com.cl.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cl.entity.SysUser;

import java.util.List;

/**
 * @author GH
 * @description 针对表【sys_user(用户信息表)】的数据库操作Service
 * @createDate 2022-07-13 10:54:50
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 根据用户名查询用户信息
     *
     * @param username
     * @return
     */
    SysUser selectByUserName(String username);

    /**
     * 分页查询用户信息
     *
     * @param page
     * @param sysUser
     * @return
     */
    IPage<SysUser> getSysUserPage(Page page, SysUser sysUser);

    /**
     * 根据用户名查询用户信息
     *
     * @param userName
     * @return
     */
    SysUser findUserByUserName(String userName);

    /**
     * 删除用户
     *
     * @param userId
     * @return
     */
    boolean deleteById(Long userId);

    /**
     * 用户分配角色
     * @param userId
     * @param roleIds
     * @return
     */
    boolean saveUserRole(Long userId, List<Long> roleIds);
}
