package com.cl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cl.entity.SysMenu;
import com.cl.entity.vo.RoleMenuVo;

import java.util.List;

/**
* @author GH
* @description 针对表【sys_menu】的数据库操作Service
* @createDate 2022-07-18 18:03:33
*/
public interface ISysMenuService extends IService<SysMenu> {

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    List<SysMenu> selectMenuPermsByUserId(Long userId);

    /**
     * 查询菜单列表
     * @param sysMenu
     * @return
     */
    List<SysMenu> getMenuList(SysMenu sysMenu);

    /**
     * 查询上级菜单列表
     * @return
     */
    List<SysMenu> getParentMenu();

    /**
     * 检查菜单下是否有子菜单
     * @param menuId
     * @return
     */
    boolean hasChildrenOfMenu(Long menuId);

    /**
     * 查询分配权限树列表
     * @param userId
     * @param roleId
     * @return
     */
    RoleMenuVo getAssignMenuTree(Long userId, Long roleId);
}
