package com.cl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cl.entity.SysRoleMenu;

/**
* @author GH
* @description 针对表【sys_role_menu(角色和菜单关联表)】的数据库操作Service
* @createDate 2022-07-13 10:54:44
*/
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
