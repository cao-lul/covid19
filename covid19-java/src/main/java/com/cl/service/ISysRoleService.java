package com.cl.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cl.entity.SysRole;
import com.cl.entity.dto.RoleMenuDto;

import java.util.List;

/**
* @author GH
* @description 针对表【sys_role(角色信息表)】的数据库操作Service
* @createDate 2022-07-13 10:53:51
*/
public interface ISysRoleService extends IService<SysRole> {

    /**
     * 根据用户查询角色列表
     * @param page
     * @param sysRole
     * @return
     */
    IPage<SysRole> getRolePage(Page page, SysRole sysRole);

    /**
     * 检查角色是否被分配出去
     * @param roleId
     * @return
     */
    boolean checkRole(Long roleId);

    /**
     * 删除角色
     * @param roleId
     * @return
     */
    boolean deleteByRoleId(Long roleId);

    /**
     * 保存权限数据
     * @param roleMenuDto
     * @return
     */
    boolean saveRoleMenu(RoleMenuDto roleMenuDto);

    /**
     * 根据用户id查询该用户所拥有的角色id
     * @param userId
     * @return
     */
    List<Long> findRoleIdByUserId(Long userId);
}
