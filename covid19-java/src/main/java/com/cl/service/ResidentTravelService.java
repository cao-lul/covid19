package com.cl.service;

import com.cl.entity.ResidentTravel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 常驻人口外出归来行程表 服务类
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
public interface ResidentTravelService extends IService<ResidentTravel> {

}
