package com.cl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cl.entity.SysUserRole;

/**
* @author GH
* @description 针对表【sys_user_role(用户和角色关联表)】的数据库操作Service
* @createDate 2022-07-13 10:54:54
*/
public interface ISysUserRoleService extends IService<SysUserRole> {

}
