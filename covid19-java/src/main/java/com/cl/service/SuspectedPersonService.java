package com.cl.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.SuspectedPerson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 疑似人员表 服务类
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
public interface SuspectedPersonService extends IService<SuspectedPerson> {

    List<SuspectedPerson> querySusList(QueryWrapper<SuspectedPerson> wrapper);
}
