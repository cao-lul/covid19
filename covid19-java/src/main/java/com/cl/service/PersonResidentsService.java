package com.cl.service;

import com.cl.entity.PersonResidents;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【person_residents(常驻人口表)】的数据库操作Service
* @createDate 2022-11-14 20:45:02
*/
public interface PersonResidentsService extends IService<PersonResidents> {

}
