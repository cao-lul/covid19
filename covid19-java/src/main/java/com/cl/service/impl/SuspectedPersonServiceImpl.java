package com.cl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.SuspectedPerson;
import com.cl.mapper.SuspectedPersonMapper;
import com.cl.service.SuspectedPersonService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 疑似人员表 服务实现类
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Service
public class SuspectedPersonServiceImpl extends ServiceImpl<SuspectedPersonMapper, SuspectedPerson> implements SuspectedPersonService {

    @Resource
    private SuspectedPersonMapper suspectedPersonMapper;

    @Override
    public List<SuspectedPerson> querySusList(QueryWrapper<SuspectedPerson> wrapper) {
        return suspectedPersonMapper.querySusList(wrapper);
    }
}
