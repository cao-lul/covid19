package com.cl.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cl.constant.Constants;
import com.cl.mapper.SysUserMapper;
import com.cl.service.ISysUserService;
import com.cl.entity.SysUser;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author GH
 * @description 针对表【sys_user(用户信息表)】的数据库操作Service实现
 * @createDate 2022-07-13 10:54:50
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    /**
     * 根据用户名查询用户信息
     *
     * @param username
     * @return
     */
    @Override
    @DS("master")
    public SysUser selectByUserName(String username) {
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>();
        queryWrapper.eq("user_name", username);
        SysUser user = baseMapper.selectOne(queryWrapper);
        return user;
    }

    /**
     * 分页查询用户信息
     *
     * @param page
     * @param sysUser
     * @return
     */
    @Override
    public IPage<SysUser> getSysUserPage(Page page, SysUser sysUser) {
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        //部门编号
        queryWrapper.eq(!ObjectUtils.isEmpty(sysUser.getDeptId()), "dept_id", sysUser.getDeptId());
        //用户名
        queryWrapper.like(!ObjectUtils.isEmpty(sysUser.getUserName()), "user_name", sysUser.getUserName());
        //用户昵称
        queryWrapper.like(!ObjectUtils.isEmpty(sysUser.getNickName()), "nick_name", sysUser.getNickName());
        //电话
        queryWrapper.like(!ObjectUtils.isEmpty(sysUser.getPhonenumber()), "phonenumber", sysUser.getPhonenumber());
        return baseMapper.selectPage(page, queryWrapper);
    }

    /**
     * 根据用户名查询用户信息
     *
     * @param userName
     * @return
     */
    @Override
    public SysUser findUserByUserName(String userName) {
        return baseMapper.selectOne(new QueryWrapper<SysUser>().eq("user_name", userName));
    }

    /**
     * 删除用户
     *
     * @param userId
     * @return
     */
    @Override
    public boolean deleteById(Long userId) {
        //查询
        SysUser user = baseMapper.selectById(userId);
        //删除用户角色关系
        baseMapper.deleteUserRole(userId);
        //删除用户
        if (baseMapper.deleteById(userId) > 0) {
            //判断用户是否存在
            if (user != null && !ObjectUtils.isEmpty(user.getAvatar()) && !user.getAvatar().equals(Constants.DEFAULT_AVATAR)) {
                //TODO 删除阿里云头像
                //fileService.deleteFile(user.getAvatar());
            }
            return true;
        }
        return false;
    }

    /**
     * 用户分配角色
     *
     * @param userId
     * @param roleIds
     * @return
     */
    @Override
    public boolean saveUserRole(Long userId, List<Long> roleIds) {
        //删除该用户对应的角色信息
        baseMapper.deleteUserRole(userId);
        //保存用户角色信息
        return baseMapper.saveUserRole(userId, roleIds) > 0;
    }
}
