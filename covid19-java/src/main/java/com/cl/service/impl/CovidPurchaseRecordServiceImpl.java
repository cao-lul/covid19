package com.cl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.cl.entity.CovidPurchaseRecord;
import com.cl.mapper.CovidPurchaseRecordMapper;
import com.cl.service.CovidPurchaseRecordService;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【covid_purchase_record(菜单信息表)】的数据库操作Service实现
* @createDate 2022-11-12 14:25:02
*/
@Service
public class CovidPurchaseRecordServiceImpl extends ServiceImpl<CovidPurchaseRecordMapper, CovidPurchaseRecord>
    implements CovidPurchaseRecordService {

}




