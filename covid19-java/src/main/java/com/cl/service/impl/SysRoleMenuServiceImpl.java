package com.cl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cl.mapper.SysRoleMenuMapper;
import com.cl.service.ISysRoleMenuService;
import com.cl.entity.SysRoleMenu;
import org.springframework.stereotype.Service;

/**
* @author GH
* @description 针对表【sys_role_menu(角色和菜单关联表)】的数据库操作Service实现
* @createDate 2022-07-13 10:54:44
*/
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

}
