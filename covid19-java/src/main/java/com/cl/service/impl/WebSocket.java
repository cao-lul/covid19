package com.cl.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
@ServerEndpoint("/websocket")
public class WebSocket {

    //存储服务连接对象
    private static Map<String, Session> clientMap = new ConcurrentHashMap<>();

    /**
     * 客户端与服务端连接成功
     *
     * @param session 会话
     */
    @OnOpen
    public void onOpen(Session session){
        log.info("【Websocket：】===============连接成功");
        clientMap.put(session.getId(),session);
    }

    /**
     * 客户端与服务端连接关闭之
     *
     * @param session 会话
     */
    @OnClose
    public void onClose(Session session){
        log.info("【Websocket：】===============关闭成功");
        clientMap.remove(session.getId());
    }

    /**
     * 连接异常
     *
     * @param error   错误
     * @param session 会话
     */
    @OnError
    public void onError(Throwable error,Session session){
        log.info("【Websocket：】===============连接异常：");
        error.printStackTrace();
    }

    /**
     * 客户端向服务端发送消息
     *
     * @param session 会话
     * @param message 消息
     */
    @OnMessage
    public void onMsg(Session session,String message){
        log.info("【Websocket：】===============客户端向服务端发送信息");
        sendAllMessage(message);
    }

    public void sendAllMessage(String message){
        Set<String> keySet = clientMap.keySet();
        for (String sessionId : keySet){
            //获取连接session
            Session session = clientMap.get(sessionId);
            //发送消息给客户端
            session.getAsyncRemote().sendText(message);
        }
    }
}
