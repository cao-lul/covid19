package com.cl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.PersonResidents;
import com.cl.entity.SysMail;
import com.cl.entity.vo.MailVo;
import com.cl.mapper.SysMailMapper;
import com.cl.service.PersonResidentsService;
import com.cl.service.SysMailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.jnlp.PersistenceService;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 邮件服务表 服务实现类
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
@Service
public class SysMailServiceImpl extends ServiceImpl<SysMailMapper, SysMail> implements SysMailService {

    @Autowired
    private PersonResidentsService personResidentsService;

    @Override
    public boolean upAndSave(List<MultipartFile> files, MailVo mailVoReal, String mailType) {
        SysMail mail = new SysMail();
        mail.setMailFrom(mailVoReal.getFrom());
        mail.setMailCc(mailVoReal.getCc());
        mail.setMailSubject(mailVoReal.getSubject());
        mail.setMailBcc(mailVoReal.getBcc());
        mail.setMailText(mailVoReal.getText());
        ArrayList<String> emailList = new ArrayList<>();
        if ("1".equals(mailType)) {
            QueryWrapper<PersonResidents> wrapper = new QueryWrapper<>();
            wrapper.eq("is_delete", 0);
            List<PersonResidents> list = personResidentsService.list(wrapper);
            if (list.size() > 0) {
                for (PersonResidents item : list) {
                    emailList.add(item.getEmail());
                }
            }
            mail.setMailTo(mailType);
        }
        return false;
    }

    public String arrToString(String[] arr) {
        String join = String.join(",", arr);
        return join;
    }

//    public static List<String> getEmail(String mailType){
//        ArrayList<String> emailList = new ArrayList<>();
//        if ("1".equals(mailType)) {
//            QueryWrapper<PersonResidents> wrapper = new QueryWrapper<>();
//            wrapper.eq("is_delete", 0);
//            List<PersonResidents> list = personResidentsService.list(wrapper);
//            if (list.size() > 0) {
//                for (PersonResidents item : list) {
//                    emailList.add(item.getEmail());
//                }
//            }
//            mail.setMailTo(mailType);
//        }
//    }
}
