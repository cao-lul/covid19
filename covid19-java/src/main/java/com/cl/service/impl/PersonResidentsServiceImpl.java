package com.cl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cl.entity.PersonResidents;
import com.cl.mapper.PersonResidentsMapper;
import com.cl.service.PersonResidentsService;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【person_residents(常驻人口表)】的数据库操作Service实现
* @createDate 2022-11-14 20:45:02
*/
@Service
public class PersonResidentsServiceImpl extends ServiceImpl<PersonResidentsMapper, PersonResidents>
    implements PersonResidentsService {

}




