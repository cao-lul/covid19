package com.cl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cl.entity.SysUserRole;
import com.cl.mapper.SysUserRoleMapper;
import com.cl.service.ISysUserRoleService;
import org.springframework.stereotype.Service;

/**
* @author GH
* @description 针对表【sys_user_role(用户和角色关联表)】的数据库操作Service实现
* @createDate 2022-07-13 10:54:54
*/
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
