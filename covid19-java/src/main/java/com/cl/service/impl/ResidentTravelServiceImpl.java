package com.cl.service.impl;

import com.cl.entity.ResidentTravel;
import com.cl.mapper.ResidentTravelMapper;
import com.cl.service.ResidentTravelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 常驻人口外出归来行程表 服务实现类
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Service
public class ResidentTravelServiceImpl extends ServiceImpl<ResidentTravelMapper, ResidentTravel> implements ResidentTravelService {

}
