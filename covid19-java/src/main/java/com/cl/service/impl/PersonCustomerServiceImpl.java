package com.cl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cl.entity.PersonCustomer;
import com.cl.mapper.PersonCustomerMapper;
import com.cl.service.PersonCustomerService;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【person_customer(来访登记表)】的数据库操作Service实现
* @createDate 2022-11-14 21:13:24
*/
@Service
public class PersonCustomerServiceImpl extends ServiceImpl<PersonCustomerMapper, PersonCustomer>
    implements PersonCustomerService {

}




