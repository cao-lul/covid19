package com.cl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.VaccinationRecord;
import com.cl.entity.query.VaccinationRecordQuery;
import com.cl.mapper.VaccinationRecordMapper;
import com.cl.service.VaccinationRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 疫苗接种记录 服务实现类
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Service
public class VaccinationRecordServiceImpl extends ServiceImpl<VaccinationRecordMapper, VaccinationRecord> implements VaccinationRecordService {

    @Resource
    private VaccinationRecordMapper vaccinationRecordMapper;

    @Override
    public List<VaccinationRecordQuery> queryList(QueryWrapper<VaccinationRecordQuery> wrapper) {
        return vaccinationRecordMapper.queryList(wrapper);
    }
}
