package com.cl.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.ClockingInfo;
import com.cl.entity.query.ClockingInfoQuery;
import com.cl.entity.vo.ClockInfoVo;
import com.cl.mapper.ClockingInfoMapper;
import com.cl.service.ClockingInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cl.service.PersonResidentsService;
import com.cl.utils.LukeUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 打卡信息表 服务实现类
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
@Service
public class ClockingInfoServiceImpl extends ServiceImpl<ClockingInfoMapper, ClockingInfo> implements ClockingInfoService {

    @Resource
    private PersonResidentsService personResidentsService;

    @Resource
    private ClockingInfoService clockingInfoService;

    @Override
    public ClockInfoVo infoReport(ClockingInfoQuery query) throws Exception {
        //查询社区总人数
        BigDecimal residentsAll = BigDecimal.valueOf(personResidentsService.count());
        //条件查询打卡人数
        QueryWrapper<ClockingInfo> wrapper = new QueryWrapper<>();
        ClockInfoVo infoVo = new ClockInfoVo();
        if (null != query.getBeginTime() && null != query.getEndTime()) {
            wrapper.ge("create_time", LukeUtils.parseStringToBegin(query.getBeginTime()));
            wrapper.le("create_time", LukeUtils.parseStringToEnd(query.getEndTime()));
        }
        wrapper.eq("is_delete", 0);
        BigDecimal clocked = BigDecimal.valueOf(clockingInfoService.count(wrapper));
        //圆环
        BigDecimal clockRate = clocked.divide(residentsAll,2,BigDecimal.ROUND_HALF_UP);
        infoVo.setClockRate(clockRate);
        //查询集合
        List<ClockingInfo> infoList = clockingInfoService.list(wrapper);
        List<String> everyDay = LukeUtils.findEveryDay(query.getBeginTime(), query.getEndTime());
        //设置x轴
        infoVo.setEveryDay(everyDay);
        List<Long> dayClocked = new ArrayList<>();
        for (String item : everyDay) {
            Date begin = LukeUtils.parseStringToBegin(item);
            Date end = LukeUtils.parseStringToEnd(item);
            long count = infoList.stream().filter(l -> l.getCreateTime().getTime() >= begin.getTime() && l.getCreateTime().getTime() <= end.getTime()).count();
            dayClocked.add(count);
        }
        infoVo.setDayClocked(dayClocked);
        infoVo.setResidentsAll(residentsAll);

        return infoVo;
    }
}
