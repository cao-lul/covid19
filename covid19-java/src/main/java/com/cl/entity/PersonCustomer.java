package com.cl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.cl.config.mybatis.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 来访登记表
 * @TableName person_customer
 */
@TableName(value ="person_customer")
@Data
@ApiModel(value = "来访人员",description = "来访人员表")
public class PersonCustomer extends BaseEntity implements Serializable {
    /**
     * Id

     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "Id")
    private Long id;

    /**
     * 来访人数

     */
    @TableField(value = "customer_number")
    @ApiModelProperty(value = "来访人数")
    private Integer customerNumber;

    /**
     * 姓名
     */
    @TableField(value = "name")
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 电话号码
     */
    @TableField(value = "phone")
    @ApiModelProperty(value = "电话号码")
    private String phone;

    /**
     * 性别(0-女，1-男)
     */
    @TableField(value = "sex")
    @ApiModelProperty(value = "性别(0-女，1-男)")
    private Integer sex;

    /**
     * 被访问-几栋几单元几号
     */
    @TableField(value = "address")
    @ApiModelProperty(value = "被访问-几栋几单元几号")
    private String address;

    /**
     * 来访时间
     */
    @TableField(value = "coming_time")
    @ApiModelProperty(value = "来访时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date comingTime;

    /**
     *  排序
     */
    @TableField(value = "order_num")
    @ApiModelProperty(value = "排序")
    private Integer orderNum;

    /**
     * 是否删除(0-未删除，1-已删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "是否删除(0-未删除，1-已删除)")
    private Integer isDelete;

    /**
     * 车牌号
     */
    @TableField(value = "car_num")
    @ApiModelProperty(value = "车牌号")
    private String carNum;

    /**
     * 被访问姓名
     */
    @TableField(value = "hostname")
    @ApiModelProperty(value = "被访问姓名")
    private String hostname;

    /**
     * 来访地址
     */
    @TableField(value = "from_address")
    @ApiModelProperty(value = "来访地址")
    private String fromAddress;

    /**
     * 创建者
     */
    @TableField(value = "create_by")
    @ApiModelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新者
     */
    @TableField(value = "update_by")
    @ApiModelProperty(value = "更新者")
    private String updateBy;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}