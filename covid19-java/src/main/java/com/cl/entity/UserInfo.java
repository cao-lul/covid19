package com.cl.entity;

import com.cl.config.mybatis.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 获取用户信息类
 *
 * @author starsea
 * @date 2022-07-18 14:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo extends BaseEntity implements Serializable {
    //用户ID
    private Long id;
    //用户名称
    private String name;
    //头像
    private String avatar;
    //介绍
    private String introduction;
    //角色权限集合
    private Object[] roles;
}
