package com.cl.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;

import java.io.Serializable;
import java.util.Date;

import com.cl.config.mybatis.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 疫苗接种记录
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="疫苗接种记录表", description="疫苗接种记录表")
public class VaccinationRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 租户号
     */
    @ApiModelProperty(value = "租户号")
    private String tenantId;

    /**
     * 乐观锁
     */
    @Version
    @ApiModelProperty(value = "乐观锁")
    private String revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @TableField(fill = FieldFill.INSERT)
    private String createdBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    @TableField(fill = FieldFill.UPDATE)
    private String updatedBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;

    /**
     * 身份证
     */
    @ApiModelProperty(value = "身份证")
    private String personId;

    /**
     * 是否接种;0-是  1-否
     */
    @ApiModelProperty(value = "是否接种;0-是  1-否")
    private Integer vaccination;

    /**
     * 接种针数
     */
    @ApiModelProperty(value = "接种针数")
    private String vaccinationNumber;

    /**
     * 接种时间
     */
    @ApiModelProperty(value = "接种时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private Date inoculationTime;
}
