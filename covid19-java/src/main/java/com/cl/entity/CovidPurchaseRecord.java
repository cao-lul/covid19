package com.cl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.cl.config.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 物资表
 * @TableName covid_purchase_record
 */
@TableName(value ="covid_purchase_record")
@Data
@ApiModel(value = "物资记录",description = "物资表")
public class CovidPurchaseRecord extends BaseEntity implements Serializable {
    /**
     * Id
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "Id")
    private Long id;

    /**
     * 物资编码
     */
    @TableField(value = "material_code")
    @ApiModelProperty(value = "物资编码")
    private String materialCode;

    /**
     * 物资名称
     */
    @TableField(value = "material_name")
    @ApiModelProperty(value = "物资名称")
    private String materialName;

    /**
     * 购买类型
     */
    @TableField(value = "type")
    @ApiModelProperty(value = "购买类型")
    private Integer type;

    /**
     * 排序
     */
    @TableField(value = "order_num")
    @ApiModelProperty(value = "排序")
    private Integer orderNum;

    /**
     * 是否删除(0-未删除，2-已删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "是否删除(0-未删除，2-已删除)")
    private Integer isDelete;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 创建者
     */
    @TableField(value = "create_by")
    @ApiModelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新者
     */
    @TableField(value = "update_by")
    @ApiModelProperty(value = "更新者")
    private String updateBy;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 单价
     */
    @TableField(value = "price")
    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    /**
     * 购买数量
     */
    @TableField(value = "number")
    @ApiModelProperty(value = "购买数量")
    private BigDecimal number;

    /**
     * 使用数量
     */
    @TableField(value = "used_number")
    @ApiModelProperty(value = "使用数量")
    private BigDecimal usedNumber;

    /**
     * 采购人
     */
    @TableField(value = "purchase_person")
    @ApiModelProperty(value = "采购人")
    private String purchasePerson;

    /**
     * 单位
     */
    @TableField(value = "unit")
    @ApiModelProperty(value = "单位")
    private String unit;

    /**
     * 电话号码
     */
    @TableField(value = "phone")
    @ApiModelProperty(value = "电话号码")
    private String phone;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private BigDecimal totalPrice;
}