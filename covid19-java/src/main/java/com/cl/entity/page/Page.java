package com.cl.entity.page;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;
import java.util.List;

public class Page implements Serializable {

    @TableField(
            exist = false
    )
    private Integer page = 1;
    @TableField(
            exist = false
    )
    private Integer limit = 10;
    @TableField(
            exist = false
    )
    private List<BaseQuery> dynamicConditions;

    public Page() {
    }

    public Integer getPage() {
        return this.page;
    }

    public Integer getLimit() {
        return this.limit;
    }

    public List<BaseQuery> getDynamicConditions() {
        return this.dynamicConditions;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public void setDynamicConditions(List<BaseQuery> dynamicConditions) {
        this.dynamicConditions = dynamicConditions;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof Page)) {
            return false;
        } else {
            Page other = (Page)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$page = this.getPage();
                    Object other$page = other.getPage();
                    if (this$page == null) {
                        if (other$page == null) {
                            break label47;
                        }
                    } else if (this$page.equals(other$page)) {
                        break label47;
                    }

                    return false;
                }

                Object this$limit = this.getLimit();
                Object other$limit = other.getLimit();
                if (this$limit == null) {
                    if (other$limit != null) {
                        return false;
                    }
                } else if (!this$limit.equals(other$limit)) {
                    return false;
                }

                Object this$dynamicConditions = this.getDynamicConditions();
                Object other$dynamicConditions = other.getDynamicConditions();
                if (this$dynamicConditions == null) {
                    if (other$dynamicConditions != null) {
                        return false;
                    }
                } else if (!this$dynamicConditions.equals(other$dynamicConditions)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof Page;
    }

    public int hashCode() {
        boolean PRIME = true;
        int result = 1;
        Object $page = this.getPage();
        result = result * 59 + ($page == null ? 43 : $page.hashCode());
        Object $limit = this.getLimit();
        result = result * 59 + ($limit == null ? 43 : $limit.hashCode());
        Object $dynamicConditions = this.getDynamicConditions();
        result = result * 59 + ($dynamicConditions == null ? 43 : $dynamicConditions.hashCode());
        return result;
    }

    public String toString() {
        return "Page(page=" + this.getPage() + ", limit=" + this.getLimit() + ", dynamicConditions=" + this.getDynamicConditions() + ")";
    }

}
