package com.cl.entity.page;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;

public class BaseQuery implements Serializable {
    @JSONField(
            name = "_field"
    )
    private String _field;
    @JSONField(
            name = "_val"
    )
    private String _val;
    @JSONField(
            name = "_condition"
    )
    private String _condition;
    @JSONField(
            name = "_join"
    )
    private String _join;
    @JSONField(
            name = "_children"
    )
    private List<BaseQuery> _children;

    public BaseQuery() {
    }

    public String get_field() {
        return this._field;
    }

    public String get_val() {
        return this._val;
    }

    public String get_condition() {
        return this._condition;
    }

    public String get_join() {
        return this._join;
    }

    public List<BaseQuery> get_children() {
        return this._children;
    }

    public void set_field(String _field) {
        this._field = _field;
    }

    public void set_val(String _val) {
        this._val = _val;
    }

    public void set_condition(String _condition) {
        this._condition = _condition;
    }

    public void set_join(String _join) {
        this._join = _join;
    }

    public void set_children(List<BaseQuery> _children) {
        this._children = _children;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof BaseQuery)) {
            return false;
        } else {
            BaseQuery other = (BaseQuery)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label71: {
                    Object this$_field = this.get_field();
                    Object other$_field = other.get_field();
                    if (this$_field == null) {
                        if (other$_field == null) {
                            break label71;
                        }
                    } else if (this$_field.equals(other$_field)) {
                        break label71;
                    }

                    return false;
                }

                Object this$_val = this.get_val();
                Object other$_val = other.get_val();
                if (this$_val == null) {
                    if (other$_val != null) {
                        return false;
                    }
                } else if (!this$_val.equals(other$_val)) {
                    return false;
                }

                label57: {
                    Object this$_condition = this.get_condition();
                    Object other$_condition = other.get_condition();
                    if (this$_condition == null) {
                        if (other$_condition == null) {
                            break label57;
                        }
                    } else if (this$_condition.equals(other$_condition)) {
                        break label57;
                    }

                    return false;
                }

                Object this$_join = this.get_join();
                Object other$_join = other.get_join();
                if (this$_join == null) {
                    if (other$_join != null) {
                        return false;
                    }
                } else if (!this$_join.equals(other$_join)) {
                    return false;
                }

                Object this$_children = this.get_children();
                Object other$_children = other.get_children();
                if (this$_children == null) {
                    if (other$_children == null) {
                        return true;
                    }
                } else if (this$_children.equals(other$_children)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof BaseQuery;
    }

    public int hashCode() {
        boolean PRIME = true;
        int result = 1;
        Object $_field = this.get_field();
        result = result * 59 + ($_field == null ? 43 : $_field.hashCode());
        Object $_val = this.get_val();
        result = result * 59 + ($_val == null ? 43 : $_val.hashCode());
        Object $_condition = this.get_condition();
        result = result * 59 + ($_condition == null ? 43 : $_condition.hashCode());
        Object $_join = this.get_join();
        result = result * 59 + ($_join == null ? 43 : $_join.hashCode());
        Object $_children = this.get_children();
        result = result * 59 + ($_children == null ? 43 : $_children.hashCode());
        return result;
    }

    public String toString() {
        return "BaseQuery(_field=" + this.get_field() + ", _val=" + this.get_val() + ", _condition=" + this.get_condition() + ", _join=" + this.get_join() + ", _children=" + this.get_children() + ")";
    }
}
