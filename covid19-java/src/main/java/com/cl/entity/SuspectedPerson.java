package com.cl.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.cl.config.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 疑似人员表
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "疑似人员表")
public class SuspectedPerson extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 身份证
     */
    @ApiModelProperty(value = "身份证")
    private String personId;

    /**
     * 疑似人员住址
     */
    @ApiModelProperty(value = "疑似人员住址")
    private String isolationAddress;

    /**
     * 疑似人员隔离住址
     */
    @ApiModelProperty(value = "疑似人员隔离住址")
    private String isolationType;

    /**
     * 体温
     */
    @ApiModelProperty(value = "体温")
    private String temperature;

    /**
     * 出行记录
     */
    @ApiModelProperty(value = "出行记录")
    private String travelPlace;

    /**
     * 是否是流动人员
     */
    @ApiModelProperty(value = "是否是流动人员")
    private Integer isMove;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 租户号
     */
    @ApiModelProperty(value = "租户号")
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁")
    private String revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_by",fill = FieldFill.INSERT)
    private String createdBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createdTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    @TableField(value = "update_by",fill = FieldFill.UPDATE)
    private String updatedBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time",fill = FieldFill.UPDATE)
    private Date updatedTime;

    @TableField(exist = false)
    private String name;

    @TableField(exist = false)
    private String address;

    @TableField(exist = false)
    private String idCard;

    @TableField(exist = false)
    private int sex;

    @TableField(exist = false)
    private String phone;

    @TableField(exist = false)
    private int age;
}
