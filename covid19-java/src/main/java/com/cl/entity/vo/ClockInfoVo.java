package com.cl.entity.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ClockInfoVo {

    /**
     * 打卡率
     */
    private BigDecimal clockRate;

    /**
     * 2022-12-23  2022-12-27之间的天数
     */
    private List<String> everyDay;

    private List<Long> dayClocked;

    private BigDecimal residentsAll;

}
