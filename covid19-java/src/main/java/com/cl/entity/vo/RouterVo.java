package com.cl.entity.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 路由菜单类
 *
 * @author starsea
 * @date 2022-07-18 14:48
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RouterVo {

    /*
    vue-element-admin路由格式如下：
    {
      path: '/permission',
      component: Layout,
      redirect: '/permission/index', //重定向地址，在面包屑中点击会重定向去的地址
      hidden: true, // 不在侧边栏显示
      alwaysShow: true, //一直显示根路由
      meta: { roles: ['admin','editor'] }, //你可以在根路由设置权限，这样它下面所有的子路由都继承了这个权限
      children: [{
        path: 'index',
        component: ()=>import('permission/index'),
        name: 'permission',
        meta: {
          title: 'permission',
          icon: 'lock', //图标
          roles: ['admin','editor'], //或者你可以给每一个子路由设置自己的权限
          noCache: true // 不会被 <keep-alive> 缓存
        }
      }]
    }
     */

    //路由地址
    private String path;
    //路由对应的组件
    private String component;
    //是否显示
    private boolean alwaysShow;
    //路由名称
    private String name;
    //路由meta信息
    private Meta meta;
    //按时间排序路由
    private int orderNum;
    //子路由
    private List<RouterVo> children = new ArrayList<RouterVo>();

    @Data
    @AllArgsConstructor
    public class Meta {
        private String title;//标题
        private String icon;//图标
        private Object[] roles;//角色列表
    }
}
