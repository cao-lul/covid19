package com.cl.entity.vo;

import com.cl.entity.SysMenu;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色分配权限数据回显vo
 *
 * @author starsea
 * @date 2022-07-25 14:13
 */
@Data
public class RoleMenuVo {

    /**
     * 菜单数据
     */
    private List<SysMenu> menuList = new ArrayList<>();

    /**
     * 该角色原有分配的菜单数据
     */
    private Object[] checkList;
}
