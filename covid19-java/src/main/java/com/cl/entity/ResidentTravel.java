package com.cl.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.cl.config.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 常驻人口外出归来行程表
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "常驻人口行程表")
public class ResidentTravel extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 身份证
     */
    @ApiModelProperty(value = "身份证")
    private String idCard;

    /**
     * 出行方式
     */
    @ApiModelProperty(value = "出行方式")
    private String travelModel;

    /**
     * 车牌号
     */
    @ApiModelProperty(value = "车牌号")
    private String carNumber;

    /**
     * 出发时间
     */
    @ApiModelProperty(value = "出发时间")
    private LocalDateTime startTime;

    /**
     * 健康码
     */
    @ApiModelProperty(value = "健康码")
    private String healthCode;

    /**
     * 行程码
     */
    @ApiModelProperty(value = "行程码")
    private String travelCode;

    /**
     * 租户号
     */
    @ApiModelProperty(value = "租户号")
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty(value = "乐观锁")
    @Version
    private String revision;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by",fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_time",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    @TableField(value = "update_by",fill = FieldFill.UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time",fill = FieldFill.UPDATE)
    private Date updateTime;


}
