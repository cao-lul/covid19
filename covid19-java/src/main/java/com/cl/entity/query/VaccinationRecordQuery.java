package com.cl.entity.query;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.cl.entity.page.Page;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 疫苗接种记录
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class VaccinationRecordQuery extends Page implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 租户号
     */
    private String tenantId;

    /**
     * 乐观锁
     */
    private String revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;

    /**
     * 主键
     */
      @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 身份证
     */
    private String personId;

    /**
     * 是否接种;0-是  1-否
     */
    private Integer vaccination;

    /**
     * 接种针数
     */
    private String vaccinationNumber;


    /**
     * 接种时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private Date inoculationTime;

    @TableField(exist = false)
    private String keyword;

    @TableField(exist = false)
    private String name;

    @TableField(exist = false)
    private String idCard;
}
