package com.cl.entity.query;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cl.entity.page.Page;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 常驻人口表
 * @TableName person_residents
 */
@TableName(value ="person_residents")
@Data
public class PersonResidentsQuery extends Page implements Serializable {
    /**
     * Id

     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 年纪
     */
    @TableField(value = "age")
    private Integer age;

    /**
     * 姓名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 电话号码
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 性别(0-女，1-男)
     */
    @TableField(value = "sex")
    private Integer sex;

    /**
     * 几栋几单元几号
     */
    @TableField(value = "address")
    private String address;

    /**
     * 入住时间
     */
    @TableField(value = "coming_time")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date comingTime;

    /**
     * 排序
     */
    @TableField(value = "order_num")
    private Integer orderNum;

    /**
     * 是否为流动人员(默认0-否，1-流动）
     */
    @TableField(value = "is_move")
    private Integer isMove;

    /**
     * 是否删除(0-未删除，1-已删除)
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    /**
     * 创建者
     */
    @TableField(value = "create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新者
     */
    @TableField(value = "update_by")
    private String updateBy;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;


    @TableField(exist = false)
    private String keyword;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}