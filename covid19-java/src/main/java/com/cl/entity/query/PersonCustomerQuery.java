package com.cl.entity.query;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cl.entity.page.Page;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 来访登记表
 * @TableName person_customer
 */
@TableName(value ="person_customer")
@Data
public class PersonCustomerQuery extends Page implements Serializable {
    /**
     * Id

     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 来访人数

     */
    @TableField(value = "customer_number")
    private Integer customerNumber;

    /**
     * 姓名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 电话号码
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 性别(0-女，1-男)
     */
    @TableField(value = "sex")
    private Integer sex;

    /**
     * 被访问-几栋几单元几号
     */
    @TableField(value = "address")
    private String address;

    /**
     * 来访时间
     */
    @TableField(value = "coming_time")
    private Date comingTime;

    /**
     *  排序
     */
    @TableField(value = "order_num")
    private Integer orderNum;

    /**
     * 是否删除(0-未删除，1-已删除)
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    /**
     * 车牌号
     */
    @TableField(value = "car_num")
    private String carNum;

    /**
     * 被访问姓名
     */
    @TableField(value = "hostname")
    private String hostname;

    /**
     * 来访地址
     */
    @TableField(value = "from_address")
    private String fromAddress;

    /**
     * 创建者
     */
    @TableField(value = "create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新者
     */
    @TableField(value = "update_by")
    private String updateBy;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

    @TableField(exist = false)
    private String keyword;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}