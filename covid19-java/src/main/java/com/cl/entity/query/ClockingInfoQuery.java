package com.cl.entity.query;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.cl.entity.page.Page;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 打卡信息表
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ClockingInfo对象", description="打卡信息表")
public class ClockingInfoQuery extends Page implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "Id	")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "联系方式")
    private String phone;

    @ApiModelProperty(value = "打卡地点")
    private String localtion;

    @ApiModelProperty(value = "体温")
    private String temperature;

    @ApiModelProperty(value = "其他症状描述")
    private String otherDescription;

    @ApiModelProperty(value = "排序字段")
    private Long sort;

    @ApiModelProperty(value = "是否删除(0-未删除，1-已删除)")
    private Integer isDelete;

    @ApiModelProperty(value = "乐观锁")
    private String revesion;

    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty(value = "备注")
    private String remark;

    @TableField(exist = false)
    private String keyword;

    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")

    private String beginTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @TableField(exist = false)
    private String endTime;


}
