package com.cl.entity.query;

import com.baomidou.mybatisplus.annotation.*;
import com.cl.entity.page.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
public class SysMailQuery extends Page implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "Id	")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "邮件发送人")
    private String mailFrom;

    @ApiModelProperty(value = "邮件接收人")
    private String mailTo;

    @ApiModelProperty(value = "邮件抄收人")
    private String mailCc;

    @ApiModelProperty(value = "邮件密送人")
    private String mailBcc;

    @ApiModelProperty(value = "邮件主题")
    private String mailSubject;

    @ApiModelProperty(value = "邮件内容")
    private String mailText;

    @ApiModelProperty(value = "邮件附件")
    private String mialFile;

    @ApiModelProperty(value = "排序字段")
    private Long sort;

    @ApiModelProperty(value = "是否删除(0-未删除，1-已删除)")
    private Integer isDelete;

    @ApiModelProperty(value = "乐观锁")
    @Version
    private String revesion;

    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty(value = "备注")
    private String remark;

    @TableField(exist = false)
    private String mailType;

}
