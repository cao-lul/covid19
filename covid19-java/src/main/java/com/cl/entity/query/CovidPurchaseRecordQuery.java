package com.cl.entity.query;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cl.entity.page.Page;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 物资表
 * @TableName covid_purchase_record
 */
@TableName(value ="covid_purchase_record")
@Data
public class CovidPurchaseRecordQuery extends Page implements Serializable {
    /**
     * Id

     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 物资编码
     */
    @TableField(value = "material_code")
    private String materialCode;

    /**
     * 物资名称
     */
    @TableField(value = "material_name")
    private String materialName;

    /**
     * 购买类型
     */
    @TableField(value = "type")
    private Integer type;

    /**
     * 排序
     */
    @TableField(value = "order_num")
    private Integer orderNum;

    /**
     * 是否删除(0-未删除，2-已删除)
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

    /**
     * 创建者
     */
    @TableField(value = "create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新者
     */
    @TableField(value = "update_by")
    private String updateBy;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 单价
     */
    @TableField(value = "price")
    private BigDecimal price;

    /**
     * 购买数量
     */
    @TableField(value = "number")
    private BigDecimal number;

    /**
     * 使用数量
     */
    @TableField(value = "used_number")
    private BigDecimal usedNumber;

    /**
     * 采购人
     */
    @TableField(value = "purchase_person")
    private String purchasePerson;

    /**
     * 单位
     */
    @TableField(value = "unit")
    private String unit;

    /**
     * 电话号码
     */
    @TableField(value = "phone")
    private String phone;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private String keyword;

    @TableField(exist = false)
    private BigDecimal totalNumber;
}