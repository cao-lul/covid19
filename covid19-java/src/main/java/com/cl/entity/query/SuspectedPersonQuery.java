package com.cl.entity.query;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.cl.entity.page.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 疑似人员表
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SuspectedPersonQuery extends Page implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 身份证
     */
    private String personId;

    /**
     * 疑似人员隔离住址
     */
    private String isolationAddress;

    /**
     * 疑似人员隔离住址
     */
    private String isolationType;

    /**
     * 体温
     */
    private String temperature;

    /**
     * 出行记录
     */
    private String travelPlace;

    /**
     * 是否是流动人员
     */
    private Integer isMove;

    /**
     * 备注
     */
    private String remark;

    /**
     * 租户号
     */
    private String tenantId;

    /**
     * 乐观锁
     */
    private String revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;


    @TableField(exist = false)
    private String keyword;

    @TableField(exist = false)
    private String name;

}
