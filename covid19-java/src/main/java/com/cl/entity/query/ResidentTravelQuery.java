package com.cl.entity.query;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.cl.entity.page.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 常驻人口外出归来行程表
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ResidentTravelQuery extends Page implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
      @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 身份证
     */
    private String idCard;

    /**
     * 出行方式
     */
    private String travelModel;

    /**
     * 车牌号
     */
    private String carNumber;

    /**
     * 出发时间
     */
    private LocalDateTime startTime;

    /**
     * 健康码
     */
    private String healthCode;

    /**
     * 行程码
     */
    private String travelCode;

    /**
     * 租户号
     */
    private String tenantId;

    /**
     * 乐观锁
     */
    private String revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;


}
