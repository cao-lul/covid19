package com.cl.entity.query;

import lombok.Data;

@Data
public class Province {
    private String totalCured;
    private String totalDeath;
    private String totalIncrease;
    private String childStatistic;
    private City[] cityArray;
    private String totalDoubtful;
    private String totalConfirmed;

}
