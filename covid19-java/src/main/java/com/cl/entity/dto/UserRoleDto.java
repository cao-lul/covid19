package com.cl.entity.dto;

import lombok.Data;

import java.util.List;

/**
 * 用于给用户分配角色时保存选中的角色数据
 *
 * @author starsea
 * @date 2022-07-26 14:27
 */
@Data
public class UserRoleDto {
    private Long userId;
    private List<Long> roleIds;
}
