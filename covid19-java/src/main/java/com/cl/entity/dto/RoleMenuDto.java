package com.cl.entity.dto;

import lombok.Data;

import java.util.List;

/**
 * 用于给角色分配权限时保存选中的权限数据
 * @author starsea
 * @date 2022-07-25 16:17
 */
@Data
public class RoleMenuDto {
    /**
     * 角色编号
     */
    private Long roleId;

    /**
     * 权限菜单id集合
     */
    private List<Long> list;
}
