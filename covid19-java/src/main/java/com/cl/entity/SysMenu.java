package com.cl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.cl.config.mybatis.BaseEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单信息表
 *
 * @TableName sys_menu
 */
@TableName(value ="sys_menu")
@Data
public class SysMenu extends BaseEntity {
    /**
     * 权限编号
     */
    @TableId(type = IdType.AUTO)
    private Long menuId;

    /**
     * 权限名称
     */
    private String label;

    /**
     * 父权限ID
     */
    private Long parentId;

    /**
     * 父权限名称
     */
    private String parentName;

    /**
     * 授权标识符
     */
    private String code;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 路由名称
     */
    private String name;

    /**
     * 授权路径
     */
    private String url;

    /**
     * 权限类型(0-目录 1-菜单 2-按钮)
     */
    private Integer type;

    /**
     * 图标
     */
    private String icon;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否删除(0-未删除 2-已删除)
     */
    private Integer isDelete;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 是否展开
     */
    @TableField(exist = false)
    private Boolean open;

    /**
     * 子菜单列表
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)//属性值为null不进行序列化操作
    @TableField(exist = false)
    private List<SysMenu> children = new ArrayList<SysMenu>();
}