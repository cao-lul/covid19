package com.cl.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.cl.config.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 常驻人口表
 * @TableName person_residents
 */
@TableName(value ="person_residents")
@Data
@ApiModel(value = "常驻人口表")
public class PersonResidents extends BaseEntity implements Serializable {
    /**
     * Id

     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 年纪
     */
    @TableField(value = "age")
    @ApiModelProperty(value = "年纪")
    private Integer age;

    /**
     * 姓名
     */
    @TableField(value = "name")
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 电话号码
     */
    @TableField(value = "phone")
    @ApiModelProperty(value = "电话号码")
    private String phone;

    /**
     * 性别(0-女，1-男)
     */
    @TableField(value = "sex")
    @ApiModelProperty(value = "性别(0-女，1-男)")
    private Integer sex;

    /**
     * 几栋几单元几号
     */
    @TableField(value = "address")
    @ApiModelProperty(value = "几栋几单元几号")
    private String address;

    /**
     * 入住时间
     */
    @TableField(value = "coming_time")
    @ApiModelProperty(value = "入住时间")
    private Date comingTime;

    /**
     * 排序
     */
    @TableField(value = "order_num")
    @ApiModelProperty(value = "排序")
    private Integer orderNum;

    /**
     * 是否为流动人员(默认0-否，1-流动）
     */
    @TableField(value = "is_move")
    @ApiModelProperty(value = "是否为流动人员(默认0-否，1-流动）")
    private Integer isMove;

    /**
     * 是否删除(0-未删除，1-已删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "是否删除(0-未删除，1-已删除)")
    private Integer isDelete;

    /**
     * 创建者
     */
    @TableField(value = "create_by",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新者
     */
    @TableField(value = "update_by",fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新者")
    private String updateBy;

    /**
     * 修改时间
     */
    @TableField(value = "update_time",fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "邮箱")
    @TableField(value = "email")
    private String email;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}