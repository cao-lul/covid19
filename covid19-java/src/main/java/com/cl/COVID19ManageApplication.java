package com.cl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主启动类
 *
 * @author cl
 */
@MapperScan(basePackages = {"com.cl.mapper","com.cl.modules.**.mapper"})
@SpringBootApplication
public class COVID19ManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(COVID19ManageApplication.class, args);
        System.out.println("社区疫情管理系统启动成功.....");
    }

}
