package com.cl.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.SuspectedPerson;
import com.cl.entity.VaccinationRecord;
import com.cl.entity.query.VaccinationRecordQuery;
import com.cl.security.domain.LoginUser;
import com.cl.service.VaccinationRecordService;
import com.cl.utils.LoginUserUtils;
import com.cl.utils.Result;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.xml.ws.Response;
import java.util.List;

/**
 * <p>
 * 疫苗接种记录 前端控制器
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Slf4j
@RestController
@RequestMapping("/api/vaccinationRecord")
@Api(value = "疫苗接种记录接口",tags = "疫苗接种记录接口")
public class VaccinationRecordController {

    @Resource
    private VaccinationRecordService vaccinationRecordService;

    @ApiOperation("分页查询来访人员信息")
    @PostMapping("/page")
    public Result page(@RequestBody VaccinationRecordQuery query){
        PageHelper.startPage(query.getPage(),query.getLimit());
        QueryWrapper<VaccinationRecordQuery> wrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(query.getKeyword())){
            wrapper.like("name",query.getKeyword());
        }

        List<VaccinationRecordQuery> list = this.vaccinationRecordService.queryList(wrapper);
        PageInfo<VaccinationRecordQuery> pageInfo = new PageInfo<>(list);
        return Result.ok(pageInfo);
    }

    @ApiOperation("新增或修改疑似人员")
    @PostMapping("save")
    public Result save(@RequestBody VaccinationRecord suspectedPerson){
        boolean flag = false;
        if(null == suspectedPerson.getId()){
            flag = this.vaccinationRecordService.save(suspectedPerson);
        }else {
            LoginUser loginUser = LoginUserUtils.getLoginUser();
            QueryWrapper<VaccinationRecord> objectQueryWrapper = new QueryWrapper<>();
            objectQueryWrapper.eq("id",suspectedPerson.getId());
            suspectedPerson.setUpdatedBy(String.valueOf(loginUser.getUserId()));
            flag =this.vaccinationRecordService.update(suspectedPerson,objectQueryWrapper);
        }
        return Result.ok(flag);
    }
}

