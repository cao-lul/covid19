package com.cl.controller;

import com.cl.entity.SysDept;
import com.cl.service.ISysDeptService;
import com.cl.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 部门 控制器
 *
 * @author cl
 * @date 2022-07-13 10:39
 */
@Slf4j
@RestController
@RequestMapping("/system/dept")
@Api(value = "人员管理接口",tags = "人员管理接口")
public class SysDeptController {

    @Autowired
    private ISysDeptService sysDeptService;

    /**
     * 查询部门列表
     */
    @ApiOperation("查询部门列表")
    @GetMapping("/list")
    public Result listDept(SysDept sysDept) {
        List<SysDept> deptList = sysDeptService.getDeptList(sysDept);
        return Result.ok(deptList);
    }

    /**
     * 查询上级部门列表
     *
     * @return
     */
    @ApiOperation("查询上级部门列表")
    @GetMapping("/parent/list")
    public Result listParentDept() {
        List<SysDept> deptList = sysDeptService.getParentDept();
        return Result.ok(deptList);
    }

    /**
     * 添加部门
     *
     * @param sysDept
     * @return
     */
    //@PreAuthorize("hasAuthority('sys:dept:add')")
    @ApiOperation("添加部门")
    @PostMapping("/add")
    public Result addDept(@RequestBody SysDept sysDept) {
        boolean save = sysDeptService.save(sysDept);
        if (save) {
            return Result.ok().message("部门添加成功");
        }
        return Result.fail().message("部门添加失败");
    }

    /**
     * 修改部门
     *
     * @param sysDept
     * @return
     */
    //@PreAuthorize("hasAuthority('sys:dept:edit')")
    @ApiOperation("修改部门")
    @PutMapping("/update")
    public Result updateDept(@RequestBody SysDept sysDept) {
        boolean update = sysDeptService.updateById(sysDept);
        if (update) {
            return Result.ok().message("部门修改成功");
        }
        return Result.fail().message("部门修改失败");
    }

    /**
     * 查询某个部门下是否存在子部门
     *
     * @param deptId
     * @return
     */
    @ApiOperation("查询某个部门下是否存在子部门")
    @GetMapping("/check/{deptId}")
    public Result checkDept(@PathVariable Long deptId) {
        //查询部门下是否存在子部门
        boolean deptFlag = sysDeptService.selectChildrenDept(deptId);
        if (deptFlag) {
            return Result.fail().message("该部门下存在子部门，无法删除");
        }
        //查询部门下是否存在用户
        boolean userFlag = sysDeptService.selectUserDept(deptId);
        if (userFlag) {
            return Result.fail().message("该部门下存在用户，无法删除");
        }
        return Result.ok();
    }

    /**
     * 删除部门
     *
     * @param deptId
     * @return
     */
    //@PreAuthorize("hasAuthority('sys:dept:delete')")
    @ApiOperation("删除部门")
    @DeleteMapping("/delete/{deptId}")
    public Result deleteDept(@PathVariable Long deptId) {
        boolean remove = sysDeptService.removeById(deptId);
        if (remove) {
            return Result.ok().message("部门删除成功");
        }
        return Result.fail().message("部门删除失败");
    }
}
