package com.cl.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.PersonResidents;
import com.cl.entity.SuspectedPerson;
import com.cl.entity.query.PersonResidentsQuery;
import com.cl.entity.query.SuspectedPersonQuery;
import com.cl.security.domain.LoginUser;
import com.cl.service.PersonResidentsService;
import com.cl.service.SuspectedPersonService;
import com.cl.utils.LoginUserUtils;
import com.cl.utils.Result;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 疑似人员表 前端控制器
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Slf4j
@RestController
@RequestMapping("/api/suspectedPerson")
@Api(value = "疑似人员接口",tags = "疑似人员接口")
public class SuspectedPersonController {

    @Resource
    private SuspectedPersonService suspectedPersonService;

    /**
     * 分页查询
     *
     * @param query 查询
     * @return {@link Result}
     */
    @ApiOperation("分页查询疑似人员")
    @PostMapping("/page")
    public Result page(@RequestBody SuspectedPersonQuery query){
        PageHelper.startPage(query.getPage(),query.getLimit());
        QueryWrapper<SuspectedPerson> wrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(query.getKeyword())){
            wrapper.and(c->c.like("name",query.getKeyword())
                    .or().like("address",query.getKeyword()));
        }
        if(null != query.getIsMove()){
            wrapper.eq("is_move",query.getIsMove());
        }
        if(StringUtils.isNotEmpty(query.getIsolationType())){
            wrapper.eq("isolation_type",query.getIsolationType());
        }
        List<SuspectedPerson> list = this.suspectedPersonService.querySusList(wrapper);
        PageInfo<SuspectedPerson> pageInfo = new PageInfo<>(list);
        return Result.ok(pageInfo).code(200);
    }

    /**
     * 新怎常住人口
     *
     * @param suspectedPerson 常住居民
     * @return {@link Result}
     */
    @ApiOperation("新增或修改疑似人员")
    @PostMapping("save")
    public Result save(@RequestBody SuspectedPerson suspectedPerson){
        SuspectedPerson personResidents1 = new SuspectedPerson();
        boolean flag = false;
        if(null == suspectedPerson.getId()){
            flag = this.suspectedPersonService.save(suspectedPerson);
        }else {
            LoginUser loginUser = LoginUserUtils.getLoginUser();
            QueryWrapper<SuspectedPerson> objectQueryWrapper = new QueryWrapper<>();
            objectQueryWrapper.eq("id",suspectedPerson.getId());
            suspectedPerson.setUpdatedBy(String.valueOf(loginUser.getUserId()));
            flag =this.suspectedPersonService.update(suspectedPerson,objectQueryWrapper);
        }
        return Result.ok(flag);
    }

    @ApiOperation("根据id查询数据做修改")
    @PostMapping("/detail")
    public Result getByID(@RequestBody String jsonString){
        String id = JSONObject.parseObject(jsonString).getString("id");
        SuspectedPerson suspectedPerson = this.suspectedPersonService.getById(id);
        return Result.ok(suspectedPerson);
    }

    @ApiOperation("删除数据")
    @PostMapping("/delete")
    public Result deleteByID(@RequestBody String jsonString){
        String id = JSONObject.parseObject(jsonString).getString("id");
        boolean removeById = this.suspectedPersonService.removeById(id);
        return Result.ok().code(200).message("删除成功");
    }

    @ApiOperation("批量删除数据")
    @PostMapping("/deleteBatch")
    public Result deleteBatchByID(@RequestBody String jsonString){
        List<Long> ids = JSONObject.parseObject(jsonString).getJSONArray("ids").toJavaList(Long.class);
        boolean removeById = this.suspectedPersonService.removeByIds(ids);
        if(removeById) {
            return Result.ok().code(200).message("删除成功");
        }
        return Result.fail();
    }

}

