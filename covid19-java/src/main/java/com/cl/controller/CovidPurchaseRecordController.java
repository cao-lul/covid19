package com.cl.controller;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.CovidPurchaseRecord;
import com.cl.entity.query.CovidPurchaseRecordQuery;
import com.cl.security.domain.LoginUser;
import com.cl.service.CovidPurchaseRecordService;
import com.cl.utils.LoginUserUtils;
import com.cl.utils.Result;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * covid购买记录控制器
 *
 * @author caolu
 * @date 2022/11/12
 */
@Slf4j
@RestController
@RequestMapping("/api/covid_purchase_record")
@Api(value = "物资购买记录接口",tags = "物资购买记录接口")
public class CovidPurchaseRecordController {

    @Resource
    private CovidPurchaseRecordService covidPurchaseRecordService;

    @ApiOperation("购买记录分页查询")
    @PostMapping("/page")
    public Result page(@RequestBody CovidPurchaseRecordQuery query){
        PageHelper.startPage(query.getPage(),query.getLimit());
        QueryWrapper<CovidPurchaseRecord> wrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(query.getKeyword())){
            wrapper.and(c->c.like("material_name",query.getKeyword()).or().like("purchase_person",query.getKeyword()));
        }
        List<CovidPurchaseRecord> list = this.covidPurchaseRecordService.list(wrapper);
        for (CovidPurchaseRecord covidPurchaseRecord : list){
            BigDecimal number = covidPurchaseRecord.getNumber();
            BigDecimal price = covidPurchaseRecord.getPrice();
            covidPurchaseRecord.setTotalPrice(number.multiply(price));
        }
        PageInfo<CovidPurchaseRecord> pageInfo = new PageInfo<>(list);
        return Result.ok(pageInfo).code(200);
    }

    @ApiOperation("新增购买记录")
    @PostMapping("/save")
    public Result save(@RequestBody CovidPurchaseRecord covidPurchaseRecord){
        CovidPurchaseRecord purchaseRecord = new CovidPurchaseRecord();
        boolean flag = false;
        if(null == covidPurchaseRecord.getId()){
            flag = this.covidPurchaseRecordService.save(covidPurchaseRecord);
        }else {
            LoginUser loginUser = LoginUserUtils.getLoginUser();
            QueryWrapper<CovidPurchaseRecord> wrapper = new QueryWrapper<>();
            wrapper.eq("id",covidPurchaseRecord.getId());
            covidPurchaseRecord.setUpdateBy(String.valueOf(loginUser.getUserId()));
            flag = this.covidPurchaseRecordService.update(covidPurchaseRecord,wrapper);
        }
        return Result.ok(flag);
    }

    @ApiOperation("根据id查询数据")
    @PostMapping("/detail")
    public Result getByID(@RequestBody String jsonString){
        String id = JSONObject.parseObject(jsonString).getString("id");
        CovidPurchaseRecord covidPurchaseRecord = this.covidPurchaseRecordService.getById(id);
        return Result.ok(covidPurchaseRecord);
    }

    @ApiOperation("删除数据")
    @PostMapping("/delete")
    public Result deleteByID(@RequestBody String jsonString){
        String id = JSONObject.parseObject(jsonString).getString("id");
        boolean removeById = this.covidPurchaseRecordService.removeById(id);
        Assert.isTrue(removeById);
        return Result.ok().code(200).message("删除成功");
    }

    @ApiOperation("删除数据")
    @PostMapping("/deleteBatch")
    public Result deleteBatchByID(@RequestBody String jsonString){
        List<Long> ids = JSONObject.parseObject(jsonString).getJSONArray("ids").toJavaList(Long.class);
        boolean removeById = this.covidPurchaseRecordService.removeByIds(ids);
        Assert.isTrue(removeById);
        return Result.ok().code(200).message("删除成功");
    }
}
