package com.cl.controller;


import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.PersonCustomer;
import com.cl.entity.PersonResidents;
import com.cl.entity.query.PersonCustomerQuery;
import com.cl.security.domain.LoginUser;
import com.cl.service.PersonCustomerService;
import com.cl.utils.LoginUserUtils;
import com.cl.utils.Result;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.lettuce.core.pubsub.RedisPubSubListener;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 访客处理类
 *
 * @author caolu
 * @date 2022/12/22
 */
@Slf4j
@RestController
@RequestMapping("/api/person_customer")
@Api(value = "来访人员管理接口",tags = "来访人员管理接口")
public class PersonCustomerController {

    @Resource
    private PersonCustomerService personCustomerService;

    @ApiOperation("分页查询来访人员信息")
    @PostMapping("/page")
    public Result page(@RequestBody PersonCustomerQuery query){
        PageHelper.startPage(query.getPage(),query.getLimit());

        QueryWrapper<PersonCustomer> wrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(query.getKeyword())) {
            wrapper.and(c -> c.like("name", query.getKeyword())
                    .or().like("address", query.getKeyword()));
        }
        if(null != query.getSex()){
            wrapper.eq("sex",query.getSex());
        }
        wrapper.orderByDesc("coming_time");
        List<PersonCustomer> list = this.personCustomerService.list(wrapper);
        PageInfo<PersonCustomer> pageInfo = new PageInfo<>(list);
        return Result.ok(pageInfo).code(200);
    }

    @ApiOperation("新增或修改常住人口")
    @PostMapping("/save")
    public Result save(@RequestBody PersonCustomer personCustomer){
        boolean flag = false;
        if(null == personCustomer.getId()){
            flag = this.personCustomerService.save(personCustomer);
        }else {
            LoginUser loginUser = LoginUserUtils.getLoginUser();
            QueryWrapper<PersonCustomer> objectQueryWrapper = new QueryWrapper<>();
            objectQueryWrapper.eq("id",personCustomer.getId());
            personCustomer.setUpdateBy(String.valueOf(loginUser.getUserId()));
            flag =this.personCustomerService.update(personCustomer,objectQueryWrapper);
        }
        return Result.ok(flag);
    }

    @ApiOperation("根据id查询数据")
    @PostMapping("/detail")
    public Result getByID(@RequestBody String jsonString){
        String id = JSONObject.parseObject(jsonString).getString("id");
        PersonCustomer personCustomer = this.personCustomerService.getById(id);
        return Result.ok(personCustomer);
    }

    @ApiOperation("删除数据")
    @PostMapping("/delete")
    public Result deleteByID(@RequestBody String jsonString){
        String id = JSONObject.parseObject(jsonString).getString("id");
        boolean removeById = this.personCustomerService.removeById(id);
        Assert.isTrue(removeById);
        return Result.ok().code(200).message("删除成功");
    }

    @ApiOperation("批量删除数据")
    @PostMapping("/deleteBatch")
    public Result deleteBatchByID(@RequestBody String jsonString){
        List<Long> ids = JSONObject.parseObject(jsonString).getJSONArray("ids").toJavaList(Long.class);
        boolean removeById = this.personCustomerService.removeByIds(ids);
        if (removeById){
            return Result.ok().code(200).message("删除成功");
        }
       return Result.fail();
    }
}
