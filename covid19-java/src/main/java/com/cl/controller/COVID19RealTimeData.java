package com.cl.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * covid19实时数据
 *
 * @author caolu
 * @date 2022/12/22
 */
@Slf4j
@RestController
@RequestMapping("/api/covid19")
public class COVID19RealTimeData {

//    @ApiOperation("疫情实时数据接口")
//    @GetMapping("/realTimeData")
//    public Result getDetail(){
//         String host = "https://ncovdata.market.alicloudapi.com";
//        String path = "/ncov/cityDiseaseInfoWithTrend";
//        String method = "GET";
//        String appcode = "9d81d3e771f247ae84d292aaaf06ca4a";
//        Map<String, String> headers = new HashMap<String, String>();
//        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
//        headers.put("Authorization", "APPCODE " + appcode);
//        Map<String, String> querys = new HashMap<String, String>();
//
//
//        try {
//            /**
//             * 重要提示如下:
//             * HttpUtils请从
//             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
//             * 下载
//             *
//             * 相应的依赖请参照
//             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
//             */
//            HttpResponse response = HttpUtil.doGet(host, path, method, headers, querys);
//            //获取response的body
//            String s = EntityUtils.toString(response.getEntity());
//            JSONObject jsonObject = JSONObject.parseObject(s);
//            String provinceArray = jsonObject.getString("provinceArray");
//
//            return Result.ok(provinceArray);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return Result.fail();
//    }
//
//    @ApiOperation("疫情热点数据")
//    @GetMapping("/epidemicHotspot")
//    public String epidemicHotspot(){
//            String url = "https://api.muxiaoguo.cn/api/epidemic?api_key=800605ce84aed771&type=epidemicHotspot";
//        String epidemicHotspot = cn.hutool.http.HttpUtil.get(url);
//        return epidemicHotspot;
//    }
}
