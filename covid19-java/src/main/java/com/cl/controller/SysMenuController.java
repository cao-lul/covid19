package com.cl.controller;

import com.cl.entity.SysMenu;
import com.cl.entity.vo.RoleMenuVo;
import com.cl.service.ISysMenuService;
import com.cl.utils.Result;
import com.cl.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜单控制器
 *
 * @author cl
 * @date 2022-07-24 16:35
 */
@Slf4j
@RestController
@RequestMapping("/system/menu")
@Api(value = "系统菜单管理接口",tags = "系统菜单管理接口")
public class SysMenuController {

    @Autowired
    private ISysMenuService sysMenuService;

    /**
     * 查询菜单列表
     *
     * @param sysMenu
     * @return
     */
    @ApiOperation("查询菜单列表")
    @GetMapping("/list")
    public Result getMenuList(SysMenu sysMenu) {
        List<SysMenu> menuList = sysMenuService.getMenuList(sysMenu);
        return Result.ok(menuList);
    }

    /**
     * 查询上级菜单列表
     *
     * @return
     */
    @ApiOperation("查询上级菜单列表")
    @GetMapping("/parent/list")
    public Result listParentMenu() {
        List<SysMenu> menuList = sysMenuService.getParentMenu();
        return Result.ok(menuList);
    }

    /**
     * 根据menuId查询菜单信息
     *
     * @param menuId
     * @return
     */
    @GetMapping("/{menuId}")
    public Result getMenuById(@PathVariable Long menuId) {
        SysMenu sysMenu = sysMenuService.getById(menuId);
        return Result.ok(sysMenu);
    }

    /**
     * 添加菜单
     *
     * @param sysMenu
     * @return
     */
    //@PreAuthorize("hasAuthority('sys:menu:add')")
    @PostMapping("/add")
    public Result add(@RequestBody SysMenu sysMenu) {
        boolean save = sysMenuService.save(sysMenu);
        if (save) {
            return Result.ok().message("菜单添加成功");
        }
        return Result.fail().message("菜单添加失败");
    }

    /**
     * 修改菜单
     *
     * @param sysMenu
     * @return
     */
    //@PreAuthorize("hasAuthority('sys:menu:edit')")
    @PutMapping("/update")
    public Result update(@RequestBody SysMenu sysMenu) {
        boolean update = sysMenuService.updateById(sysMenu);
        if (update) {
            return Result.ok().message("菜单修改成功");
        }
        return Result.fail().message("菜单修改失败1");
    }

    /**
     * 删除菜单
     *
     * @param menuId
     * @return
     */
    //@PreAuthorize("hasAuthority('sys:menu:delete')")
    @DeleteMapping("/delete/{menuId}")
    public Result delete(@PathVariable Long menuId) {
        boolean remove = sysMenuService.removeById(menuId);
        if (remove) {
            return Result.ok().message("菜单删除成功");
        }
        return Result.fail().message("菜单删除失败");
    }

    /**
     * 检查菜单下是否有子菜单
     *
     * @param menuId
     * @return
     */
    @GetMapping("/check/{menuId}")
    public Result check(@PathVariable Long menuId) {
        //判断菜单是否有子菜单
        boolean exist = sysMenuService.hasChildrenOfMenu(menuId);
        if (exist) {
            return Result.fail().message("该菜单下有子菜单，无法删除!");
        }
        return Result.ok();
    }

    /**
     * 分配权限-查询权限树数据
     *
     * @param roleId
     * @return
     */
    //@PreAuthorize("hasAuthority('sys:role:assign')")
    @ApiOperation("分配权限-查询权限树数据")
    @GetMapping("/getAssignMenuTree/{roleId}")
    public Result getAssignMenuTree(@PathVariable Long roleId) {
        Long userId = SecurityUtils.getUserId();
        RoleMenuVo roleMenuVo = sysMenuService.getAssignMenuTree(userId, roleId);
        return Result.ok(roleMenuVo);
    }
}
