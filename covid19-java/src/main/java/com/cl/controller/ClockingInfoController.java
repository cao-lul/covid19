package com.cl.controller;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.ClockingInfo;
import com.cl.entity.query.ClockingInfoQuery;
import com.cl.entity.vo.ClockInfoVo;
import com.cl.service.ClockingInfoService;
import com.cl.service.PersonResidentsService;
import com.cl.utils.LukeUtils;
import com.cl.utils.Result;
import com.cl.utils.StringUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 打卡信息表 前端控制器
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
@Api(tags = "打卡信息表")
@RestController
@RequestMapping("/api/clockingInfo")
public class ClockingInfoController {

    @Resource
    private ClockingInfoService clockingInfoService;


    @PostMapping("/page")
    @ApiOperation(value = "分页查询打卡信息",tags = "分页查询打卡信息")
    public Result page(@RequestBody ClockingInfoQuery query){
        PageHelper.startPage(query.getPage(),query.getLimit());
        QueryWrapper<ClockingInfo> wrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(query.getKeyword())){
            wrapper.and(c->c.like("name",query.getKeyword()).or().like("localtion",query.getKeyword()));
        }
        if(null!=query.getBeginTime()&&null!=query.getEndTime()){
            Date date1 = DateUtil.parse(query.getBeginTime());
            Date date2 = DateUtil.parse(query.getEndTime());
            wrapper.ge("create_time",DateUtil.beginOfDay(date1));
            wrapper.le("create_time",DateUtil.endOfDay(date2));
        }
        wrapper.orderByDesc("create_time");
        List<ClockingInfo> list = clockingInfoService.list(wrapper);
        PageInfo<ClockingInfo> info = new PageInfo<>(list);
        return Result.ok(info);
    }

    @SneakyThrows
    @PostMapping("/report")
    @ApiOperation(value = "分页查询打卡信息",tags = "分页查询打卡信息")
    public Result report(@RequestBody ClockingInfoQuery query){
        ClockInfoVo infoVo = clockingInfoService.infoReport(query);
        return Result.ok(infoVo);
    }

}

