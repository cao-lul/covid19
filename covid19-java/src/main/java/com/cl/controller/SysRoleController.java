package com.cl.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cl.entity.SysRole;
import com.cl.entity.dto.RoleMenuDto;
import com.cl.service.ISysRoleService;
import com.cl.utils.PageRequest;
import com.cl.utils.Result;
import com.cl.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * 角色控制器
 *
 * @author cl
 * @date 2022-07-13 10:57
 */
@Slf4j
@RestController
@RequestMapping("/system/role")
@Api(value = "系统角色管理接口",tags = "系统角色管理接口")
public class SysRoleController {

    @Autowired
    private ISysRoleService sysRoleService;

    /**
     * 分页查询角色列表
     *
     * @param pageRequest
     * @return
     */
    @ApiOperation("分页查询角色列表")
    @PostMapping("/page")
    public Result getRolePage(@RequestBody PageRequest<SysRole> pageRequest) {
        IPage<SysRole> page = sysRoleService.getRolePage(pageRequest.getPage(), pageRequest.getData());
        return Result.ok(page);
    }

    /**
     * 添加角色
     *
     * @param sysRole
     * @return
     */
    @PreAuthorize("hasAuthority('sys:role:add')")
    @ApiOperation("添加角色")
    @PostMapping("/add")
    public Result addRole(@RequestBody SysRole sysRole) {
        boolean save = sysRoleService.save(sysRole);
        if (save) {
            return Result.ok().message("角色添加成功");
        }
        return Result.fail().message("角色添加失败");
    }

    /**
     * 修改角色
     *
     * @param sysRole
     * @return
     */
    @PreAuthorize("hasAuthority('sys:role:edit')")
    @ApiOperation("修改角色")
    @PutMapping("/update")
    public Result updateRole(@RequestBody SysRole sysRole) {
        sysRole.setUpdateBy(SecurityUtils.getUsername());
        sysRole.setUpdateTime(new Date());
        boolean update = sysRoleService.updateById(sysRole);
        if (update) {
            return Result.ok().message("角色修改成功");
        }
        return Result.fail().message("角色修改失败");
    }

    /**
     * 检查角色是否被分配出去
     *
     * @param roleId
     * @return
     */
    @ApiOperation("检查角色是否被分配出去")
    @GetMapping("/getCheckRole/{roleId}")
    public Result getCheckRole(@PathVariable Long roleId) {
        boolean flag = sysRoleService.checkRole(roleId);
        if (flag) {
            return Result.fail().message("该角色已经分配给其他用户，无法删除！");
        }
        return Result.ok();
    }

    /**
     * 删除角色
     *
     * 分析：
     * 删除角色前需要判断该角色是否被分配出去，如果被分配给了用户使用，则提示无法删除，
     * 否则提示确认删除，确认删除的同时需要将sys_role_menu角色关系表对应的数据一同删除。
     *
     * @param roleId
     * @return
     */
    @PreAuthorize("hasAuthority('sys:role:delete')")
    @ApiOperation("删除角色")
    @DeleteMapping("/delete/{roleId}")
    public Result deleteRole(@PathVariable Long roleId) {
        boolean remove = sysRoleService.deleteByRoleId(roleId);
        if (remove) {
            return Result.ok().message("角色删除成功");
        }
        return Result.fail().message("角色删除失败");
    }

    /**
     * 分配权限-保存权限数据
     * @param roleMenuDto
     * @return
     */
    @PreAuthorize("hasAuthority('sys:role:assign')")
    @ApiOperation("分配权限-保存权限数据")
    @PostMapping("/saveRoleAssign")
    public Result saveRoleAssign(@RequestBody RoleMenuDto roleMenuDto) {
        boolean save = sysRoleService.saveRoleMenu(roleMenuDto);
        if (save) {
            return Result.ok().message("权限分配成功");
        }
        return Result.fail().message("权限分配失败");
    }

}
