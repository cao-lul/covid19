package com.cl.controller;


import cn.hutool.extra.mail.Mail;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.PersonResidents;
import com.cl.entity.SysMail;
import com.cl.entity.query.SysMailQuery;
import com.cl.entity.vo.MailVo;
import com.cl.modules.oss.entity.SysFile;
import com.cl.modules.oss.service.SysFileService;
import com.cl.service.PersonResidentsService;
import com.cl.service.SysMailService;
import com.cl.utils.GsonUtil;
import com.cl.utils.Result;
import com.cl.utils.StringUtils;
import com.cl.utils.file.MailUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 邮件服务表 前端控制器
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
@Api(tags = "邮件服务表")
@RestController
@RequestMapping("/api/sys-mail")
public class SysMailController {

    @Resource
    private SysMailService sysMailService;

    @Resource
    private SysFileService sysFileService;

    @Resource
    private PersonResidentsService personResidentsService;

    @Resource
    private MailUtil mailUtil;

    @ApiOperation(tags = "查询公告",value = "查询公告")
    @PostMapping("/attachment")
    public Result attachment(@RequestBody SysMailQuery query){
        if(StringUtils.isNotEmpty(query.getMialFile())){
            String ids = query.getMialFile();
            String s = ids.substring(0, ids.length() - 1);
            String[] strings = s.split(",");
            QueryWrapper<SysFile> wrapper = new QueryWrapper<>();
            wrapper.in("id",strings);
            List<SysFile> fileList = sysFileService.list(wrapper);
            return Result.ok(fileList);
        }
        return Result.ok().message("暂无附件");

    }

    @ApiOperation(tags = "查询历史公告",value = "查询历史公告")
    @PostMapping("/history")
    public Result history(@RequestBody SysMailQuery query){
        PageHelper.startPage(query.getPage(), query.getLimit());
        QueryWrapper<SysMail> wrapper = new QueryWrapper<>();
        List<SysMail> list = sysMailService.list();
        PageInfo<SysMail> pageInfo = new PageInfo<>(list);
        return Result.ok(pageInfo);
    }

    @ApiOperation(tags = "查询公告",value = "查询公告")
    @GetMapping("/getMail")
    public Result getMail(){
        return Result.ok(sysMailService.list());
    }

    @ApiOperation(tags = "发送邮件", value = "邮件发送服务")
    @PostMapping("/send")
    public Result sendMail(@RequestParam String mailVo, String mailType, MultipartFile[] files) {
        MailVo mailVoReal = GsonUtil.fromJson(mailVo, MailVo.class);
        mailVoReal.setFiles(files);
        List<String> emailList = new ArrayList<>();
        if ("0".equals(mailType)) {
            QueryWrapper<PersonResidents> wrapper = new QueryWrapper<>();
            wrapper.eq("is_delete", 0);
            List<PersonResidents> list = personResidentsService.list(wrapper);
            if (list.size() > 0) {
                for (PersonResidents item : list) {
                    emailList.add(item.getEmail());
                }
            }
            String[] e = new String[emailList.size()];
            String[] email = emailList.toArray(e);
            mailVoReal.setTo(email);
        }
        mailUtil.sendMail(mailVoReal);
        if (files.length > 0) {
            List<String> list = sysFileService.uploadFile(files, "mail");
            if (list.size() > 0) {
                QueryWrapper<SysFile> wrapper = new QueryWrapper<>();
                wrapper.in("oss_name", list);
                List<SysFile> sysFileList = sysFileService.list(wrapper);
                StringBuffer ids = new StringBuffer();
                for (SysFile sysFile : sysFileList) {
                    ids.append(sysFile.getId()+",");
                }
                SysMail mail = new SysMail();
                mail.setMialFile(String.valueOf(ids));
                mail.setMailFrom(mailVoReal.getFrom());
                mail.setMailCc(mailVoReal.getCc());
                mail.setMailSubject(mailVoReal.getSubject());
                mail.setMailBcc(mailVoReal.getBcc());
                mail.setMailText(mailVoReal.getText());
                if("0".equals(mailType)){
                    mail.setMailTo(mailType);
                }else {
                    mail.setMailTo(arrToString(mailVoReal.getTo()));
                }
                boolean save = sysMailService.save(mail);
                if (save) {
                    return Result.ok().message("邮件发送成功");
                }
                return Result.fail();
            }
        }
        return Result.fail();
    }

//    @ApiOperation(tags = "发送邮件", value = "邮件发送服务")
//    @PostMapping("/send")
//    public Result send(HttpServletRequest request){
//        MultipartHttpServletRequest params = (MultipartHttpServletRequest) request;
//        List<MultipartFile> files = params.getFiles("files");
//        String mailType = params.getParameter("mailType");
//        MailVo mailVoReal = GsonUtil.fromJson(params.getParameter("mailVo"), MailVo.class);
//        mailVoReal.setFiles(files.toArray(new MultipartFile[]{}));
//        if("1".equals(mailType)){
//
//        }
//        mailUtil.sendMail(mailVoReal);
//        sysMailService.upAndSave(files,mailVoReal,mailType);
//        return Result.ok();
//    }
    public String arrToString(String[] arr) {
        String join = String.join(",", arr);
        return join;
    }

}

