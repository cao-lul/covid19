package com.cl.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cl.entity.PersonResidents;
import com.cl.entity.query.PersonResidentsQuery;
import com.cl.security.domain.LoginUser;
import com.cl.service.PersonResidentsService;
import com.cl.utils.LoginUserUtils;
import com.cl.utils.Result;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 常住人口API接口
 *
 * @author caolu
 * @date 2022/11/14
 */
@Slf4j
@RestController
@RequestMapping("/api/person_residents")
@Api(value = "常驻人口接口",tags = "常驻人口接口")
public class PersonResidentsController {

    @Resource
    private PersonResidentsService personResidentsService;

    /**
     * 分页查询
     *
     * @param query 查询
     * @return {@link Result}
     */
    @ApiOperation("分页查询常住人口")
    @PostMapping("/page")
    public Result page(@RequestBody PersonResidentsQuery query){
        PageHelper.startPage(query.getPage(),query.getLimit());
        QueryWrapper<PersonResidents> wrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(query.getKeyword())){
            wrapper.and(c->c.like("name",query.getKeyword())
                    .or().like("address",query.getKeyword()));
        }
        if(null != query.getIsMove()){
            wrapper.eq("is_move",query.getIsMove());
        }
        List<PersonResidents> list = this.personResidentsService.list(wrapper);
        PageInfo<PersonResidents> pageInfo = new PageInfo<>(list);
        return Result.ok(pageInfo).code(200);
    }

    /**
     * 新怎常住人口
     *
     * @param personResidents 常住居民
     * @return {@link Result}
     */
    @ApiOperation("新增或修改常住人口")
    @PostMapping("save")
    public Result save(@RequestBody PersonResidents personResidents){
        PersonResidents personResidents1 = new PersonResidents();
        boolean flag = false;
        if(null == personResidents.getId()){
            flag = this.personResidentsService.save(personResidents);
        }else {
            LoginUser loginUser = LoginUserUtils.getLoginUser();
            QueryWrapper<PersonResidents> objectQueryWrapper = new QueryWrapper<>();
            objectQueryWrapper.eq("id",personResidents.getId());
            personResidents.setUpdateBy(String.valueOf(loginUser.getUserId()));
            flag =this.personResidentsService.update(personResidents,objectQueryWrapper);
        }
        return Result.ok(flag);
    }

    @ApiOperation("根据id查询数据做修改")
    @PostMapping("/detail")
    public Result getByID(@RequestBody String jsonString){
        String id = JSONObject.parseObject(jsonString).getString("id");
        PersonResidents personResidents = this.personResidentsService.getById(id);
        return Result.ok(personResidents);
    }

    @ApiOperation("删除数据")
    @PostMapping("/delete")
    public Result deleteByID(@RequestBody String jsonString){
        String id = JSONObject.parseObject(jsonString).getString("id");
        boolean removeById = this.personResidentsService.removeById(id);
        return Result.ok().code(200).message("删除成功");
    }

    @ApiOperation("批量删除数据")
    @PostMapping("/deleteBatch")
    public Result deleteBatchByID(@RequestBody String jsonString){
        List<Long> ids = JSONObject.parseObject(jsonString).getJSONArray("ids").toJavaList(Long.class);
        boolean removeById = this.personResidentsService.removeByIds(ids);
        if(removeById) {
            return Result.ok().code(200).message("删除成功");
        }
        return Result.fail();
    }
}
