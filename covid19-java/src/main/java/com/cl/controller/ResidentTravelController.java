package com.cl.controller;


import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 常驻人口外出归来行程表 前端控制器
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
@Slf4j
@RestController
@RequestMapping("/residentTravel")
@Api(value = "外出人员行程接口",tags = "外出人员行程接口")
public class ResidentTravelController {

}

