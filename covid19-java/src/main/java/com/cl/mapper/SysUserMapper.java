package com.cl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cl.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author GH
* @description 针对表【sys_user(用户信息表)】的数据库操作Mapper
* @createDate 2022-07-13 10:54:50
* @Entity entity.com.cl.SysUser
*/
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 删除用户角色关系
     * @param userId
     */
    void deleteUserRole(Long userId);

    /**
     * 保存用户角色关系
     * @param userId
     * @param roleIds
     * @return
     */
    int saveUserRole(@Param("userId") Long userId, @Param("roleIds") List<Long> roleIds);
}
