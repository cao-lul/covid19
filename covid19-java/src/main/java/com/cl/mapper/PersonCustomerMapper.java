package com.cl.mapper;

import com.cl.entity.PersonCustomer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【person_customer(来访登记表)】的数据库操作Mapper
* @createDate 2022-11-14 21:13:24
* @Entity generator.domain.PersonCustomer
*/
public interface PersonCustomerMapper extends BaseMapper<PersonCustomer> {

}




