package com.cl.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.cl.entity.SuspectedPerson;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 疑似人员表 Mapper 接口
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
public interface SuspectedPersonMapper extends BaseMapper<SuspectedPerson> {

    List<SuspectedPerson> querySusList(@Param(Constants.WRAPPER) QueryWrapper<SuspectedPerson> wrapper);
}
