package com.cl.mapper;

import com.cl.entity.SysMail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 邮件服务表 Mapper 接口
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
public interface SysMailMapper extends BaseMapper<SysMail> {

}
