package com.cl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cl.entity.SysDept;

/**
* @author GH
* @description 针对表【sys_dept(部门表)】的数据库操作Mapper
* @createDate 2022-07-13 10:37:37
* @Entity entity.com.cl.SysDept
*/
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
