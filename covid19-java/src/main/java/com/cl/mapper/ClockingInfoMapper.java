package com.cl.mapper;

import com.cl.entity.ClockingInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 打卡信息表 Mapper 接口
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
public interface ClockingInfoMapper extends BaseMapper<ClockingInfo> {

}
