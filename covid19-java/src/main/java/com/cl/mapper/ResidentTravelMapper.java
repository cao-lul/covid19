package com.cl.mapper;

import com.cl.entity.ResidentTravel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 常驻人口外出归来行程表 Mapper 接口
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
public interface ResidentTravelMapper extends BaseMapper<ResidentTravel> {

}
