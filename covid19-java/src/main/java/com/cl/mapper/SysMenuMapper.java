package com.cl.mapper;

import com.cl.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author GH
* @description 针对表【sys_menu】的数据库操作Mapper
* @createDate 2022-07-18 18:03:33
* @Entity entity.com.cl.SysMenu
*/
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 根据当前用户id查询自己所拥有的权限
     * @param userId
     * @return
     */
    List<SysMenu> selectMenuPermsByUserId(Long userId);

    /**
     * 查询要分配角色拥有的权限列表
     * @param roleId
     * @return
     */
    List<SysMenu> selectMenuPermsByRoleId(Long roleId);
}
