package com.cl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cl.entity.SysRoleMenu;

/**
* @author GH
* @description 针对表【sys_role_menu(角色和菜单关联表)】的数据库操作Mapper
* @createDate 2022-07-13 10:54:44
* @Entity entity.com.cl.SysRoleMenu
*/
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
