package com.cl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cl.entity.SysUserRole;

/**
* @author GH
* @description 针对表【sys_user_role(用户和角色关联表)】的数据库操作Mapper
* @createDate 2022-07-13 10:54:54
* @Entity entity.com.cl.SysUserRole
*/
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
