package com.cl.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cl.entity.CovidPurchaseRecord;

/**
* @author Administrator
* @description 针对表【covid_purchase_record(菜单信息表)】的数据库操作Mapper
* @createDate 2022-11-12 14:25:02
* @Entity generator.domain.CovidPurchaseRecord
*/
public interface CovidPurchaseRecordMapper extends BaseMapper<CovidPurchaseRecord> {

}




