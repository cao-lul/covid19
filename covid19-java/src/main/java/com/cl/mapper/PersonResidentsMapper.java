package com.cl.mapper;

import com.cl.entity.PersonResidents;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【person_residents(常驻人口表)】的数据库操作Mapper
* @createDate 2022-11-14 20:45:02
* @Entity com.cl.entity.PersonResidents
*/
public interface PersonResidentsMapper extends BaseMapper<PersonResidents> {

}




