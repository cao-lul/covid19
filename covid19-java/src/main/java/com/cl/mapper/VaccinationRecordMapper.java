package com.cl.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.cl.entity.VaccinationRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cl.entity.query.VaccinationRecordQuery;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * <p>
 * 疫苗接种记录 Mapper 接口
 * </p>
 *
 * @author cl
 * @since 2022-11-27
 */
public interface VaccinationRecordMapper extends BaseMapper<VaccinationRecord> {

    List<VaccinationRecordQuery> queryList(@Param(Constants.WRAPPER) QueryWrapper<VaccinationRecordQuery> wrapper);
}
