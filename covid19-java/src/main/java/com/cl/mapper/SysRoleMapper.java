package com.cl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cl.entity.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author GH
* @description 针对表【sys_role(角色信息表)】的数据库操作Mapper
* @createDate 2022-07-13 10:53:51
* @Entity entity.com.cl.SysRole
*/
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 根据roleId查询角色是否被分配出去
     * @param roleId
     * @return
     */
    int checkRole(@Param("roleId") Long roleId);

    /**
     * 删除角色菜单关联表
     * @param roleId
     */
    void deleteMenuByRoleId(@Param("roleId") Long roleId);

    /**
     * 删除角色菜单关系
     * @param roleId
     */
    void deleteRoleMenu(Long roleId);

    /**
     * 保存角色菜单关系
     * @param roleId
     * @param menuIds
     * @return
     */
    int saveRoleMenu(Long roleId, List<Long> menuIds);

    /**
     * 根据用户ID查询该用户拥有的角色ID
     * @param userId
     * @return
     */
    List<Long> findRoleIdByUserId(Long userId);
}
