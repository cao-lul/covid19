package com.cl.constant;

/**
 * 通用常量信息
 *
 * @author starsea
 * @date 2022-07-13 15:26
 */
public class Constants {
    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/profile";

    /**
     * 上传路径
     */
    public static final String PROFILE = "/home/authority/uploadPath";

    /**
     * 令牌
     */
    public static final String TOKEN = "token";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 获取ip地址开关
     */
    public static final boolean ADDRESSENABLED = false;

    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    /**
     * 令牌前缀
     */
    public static final String LOGIN_USER_KEY = "login_user_key";

    /**
     * 用户头像
     */
    public static final String DEFAULT_AVATAR = "";
}
