package com.cl.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 自定义异常类
 *
 * @author starsea
 * @date 2022-07-18 13:11
 */
public class CustomerAuthenticationException extends AuthenticationException {

    public CustomerAuthenticationException(String message) {
        super(message);
    }

}
