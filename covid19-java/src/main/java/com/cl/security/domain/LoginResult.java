package com.cl.security.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 认证成功处理器返回token信息
 *
 * @author starsea
 * @date 2022-07-18 12:40
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class LoginResult {
    /**
     * 用户编号
     */
    private Long id;
    /**
     * 状态码
     */
    private int code;
    /**
     * token令牌
     */
    private String token;
    /**
     * token过期时间
     */
    private Long expireTime;

    public LoginResult(String token, Long expireTime) {
        this.token = token;
        this.expireTime = expireTime;
    }
}
