package com.cl.security.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.cl.constant.CacheConstants;
import com.cl.constant.ResultCode;
import com.cl.security.domain.LoginResult;
import com.cl.security.domain.LoginUser;
import com.cl.utils.JwtUtils;
import com.cl.utils.RedisCache;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 登录认证成功处理器
 *
 * @author starsea
 * @date 2022-07-18 09:20
 */
@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private RedisCache redisCache;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        //设置客户端的响应内容类型
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        //获取当前登录用户信息
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();

        //生成token
        String token = jwtUtils.generateToken(loginUser);
        //设置token签名密钥及过期时间
        long expireTime = Jwts.parser() //获取DefaultJwtParser对象
                .setSigningKey(jwtUtils.getSecret()) //设置签名的密钥
                .parseClaimsJws(token.replace("jwt_", ""))
                .getBody().getExpiration().getTime();//获取token过期时间

        //把生成的token存到redis
        String tokenKey = CacheConstants.LOGIN_TOKEN_KEY+token;
        redisCache.set(tokenKey, token, jwtUtils.getExpireTime() / 1000);

        //封装返回信息
        LoginResult loginResult = new LoginResult(loginUser.getUserId(), ResultCode.SUCCESS, token, expireTime);
        //消除循环引用
        String result = JSON.toJSONString(loginResult, SerializerFeature.DisableCircularReferenceDetect);
        //获取输出流
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(result.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    }
}
