package com.cl.modules.oss.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cl.modules.oss.entity.SysFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 文件表 服务类
 * </p>
 *
 * @author caolu
 * @since 2023-03-03
 */
public interface SysFileService extends IService<SysFile> {

    public List<String> uploadFile(MultipartFile[] files, String prefix);

}
