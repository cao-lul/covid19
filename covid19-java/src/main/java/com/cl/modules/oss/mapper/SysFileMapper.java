package com.cl.modules.oss.mapper;

import com.cl.modules.oss.entity.SysFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文件表 Mapper 接口
 * </p>
 *
 * @author caolu
 * @since 2023-03-03
 */
public interface SysFileMapper extends BaseMapper<SysFile> {

}
