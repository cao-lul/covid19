package com.cl.modules.oss.controller;


import com.cl.exception.ServiceException;
import com.cl.modules.oss.entity.SysFile;
import com.cl.modules.oss.service.SysFileService;
import com.cl.modules.oss.utils.OssUtil;
import com.cl.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <p>
 * 文件表 前端控制器
 * </p>
 *
 * @author caolu
 * @since 2023-03-03
 */
@RestController
@RequestMapping("/sys-file")
@Api(tags = "文件服务")
public class SysFileController {
    @Autowired
    private OssUtil ossUtil;
    @Autowired
    private SysFileService sysFileService;

    @ApiOperation(value = "上传文件")
    @PostMapping("/upload")
    public Result upload(@ApiParam MultipartFile file) {
        // 用于保存文件 url
        String url = null;
        // 用于保存文件信息
        SysFile backOss = new SysFile();
        try {
            // 获取文件上传路径
            url = ossUtil.uploadSuffix(file.getInputStream(), "aliyun", file.getOriginalFilename());
            // 保存文件路径到数据库中
            backOss.setFileName(file.getOriginalFilename());
            backOss.setOssName(url);
            backOss.setFileUrl(ossUtil.getUrl(url));
            sysFileService.save(backOss);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Result.ok(backOss).message("文件上传成功");
    }

    @ApiOperation(value = "获取所有文件信息")
    @GetMapping("/getAll")
    public Result getAll() {
        return Result.ok(sysFileService.list());
    }
}

