package com.cl.modules.oss.service.impl;

import com.cl.modules.oss.entity.SysFile;
import com.cl.modules.oss.mapper.SysFileMapper;
import com.cl.modules.oss.service.SysFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cl.modules.oss.utils.OssUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 文件表 服务实现类
 * </p>
 *
 * @author caolu
 * @since 2023-03-03
 */
@Service
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFile> implements SysFileService {

    @Autowired
    private OssUtil ossUtil;

    @Autowired
    private SysFileService sysFileService;

    public List<String> uploadFile(MultipartFile[] files, String prefix) {
        String url = "";
        SysFile sysFile = new SysFile();
        ArrayList<String> urlList = new ArrayList<>();
        try {
            for (MultipartFile file : files) {
                // 获取文件上传路径
                url = ossUtil.uploadSuffix(file.getInputStream(), prefix, file.getOriginalFilename());
                // 保存文件路径到数据库中
                sysFile.setFileName(file.getOriginalFilename());
                sysFile.setOssName(url);
                sysFile.setFileUrl(ossUtil.getUrl(url));
                sysFileService.save(sysFile);
                urlList.add(url);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return urlList;
    }

}
