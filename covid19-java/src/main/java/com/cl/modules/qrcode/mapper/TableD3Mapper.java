package com.cl.modules.qrcode.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cl.modules.qrcode.entity.TableD1;
import com.cl.modules.qrcode.entity.TableD3;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
public interface TableD3Mapper extends BaseMapper<TableD3> {

    @DS("slave1")
    public int punchInIndex();

    @DS("slave1")
    public void resetPunch();

    @DS("slave1")
    public List<TableD3> listNewPunch(@Param("limits") int limits);

}
