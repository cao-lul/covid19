package com.cl.modules.qrcode.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cl.modules.qrcode.entity.TableD1;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
public interface TableD1Mapper extends BaseMapper<TableD1> {

    @DS("slave1")
    public int customerIndex();

    @DS("slave1")
    public void resetIndex();

    @DS("slave1")
    public List<TableD1> listNew(@Param("limits") int limits);
}
