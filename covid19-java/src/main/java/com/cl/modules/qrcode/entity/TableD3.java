package com.cl.modules.qrcode.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TableD3对象", description="TableD3对象")
@TableName(value = "table_D3")
@AllArgsConstructor
public class TableD3 implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "record_id", type = IdType.ASSIGN_ID)
    private Long recordId;

    private Long codeId;

    private Long tplCodeId;

    @TableField(value = "码名称")
    private String codeName;

    private Long tplId;

    private String 记录单名称;

    @TableField(value = "记录时间")
    private Date recordTime;

    private String 记录人;

    private String 状态;

    private Long memberId;

    private Long authId;

    private String 记录编号;

    private String 处理状态;

    private String 创建来源;

    private String 来源编号;

    private String 提交编号;

    private Date 处理状态变更时间;

    @TableField(value = "姓名_1858957")
    private String name;

    @TableField(value = "手机_1858958")
    private String phone;

    @TableField(value = "体温_1858960")
    private String temperature;

    @TableField(value = "定位_address_1858959")
    private String location;

    @TableField(value = "定位_lat_1858959")
    private String latitude;

    @TableField(value = "定位_log_1858959")
    private String longitude;

    @TableField(value = "其他症状描述_1858961")
    private String otherDescription;




}
