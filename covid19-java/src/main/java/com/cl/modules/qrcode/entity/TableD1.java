package com.cl.modules.qrcode.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author caolu
 * @since 2023-03-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TableD1对象", description="来访记录")
@TableName(value = "table_D1")
@AllArgsConstructor
public class TableD1 implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "record_id", type = IdType.ASSIGN_ID)
    private Long recordId;

    private Long codeId;

    private Long tplCodeId;

    @TableField(value = "码名称")
    private String codeName;

    private Long tplId;

    private String 记录单名称;

    @TableField(value = "记录时间")
    private Date recordTime;

    private String 记录人;

    private String 状态;

    private Long memberId;

    private Long authId;

    private String 记录编号;

    private String 处理状态;

    private String 创建来源;

    private String 来源编号;

    private String 提交编号;

    private Date 处理状态变更时间;

    @TableField(value = "姓名_377404")
    private String name;

    @TableField(value = "手机_377405")
    private String phone;

    @TableField(value = "受访人姓名_377406")
    private String hostName;

    @TableField(value = "详细住址_1858898")
    private String address;

    @TableField(value = "来访事由_377407")
    private String reason;

    @TableField(value = "车牌号_1864115")
    private String carNum;
}
