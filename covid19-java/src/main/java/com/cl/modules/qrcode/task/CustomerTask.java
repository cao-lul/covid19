package com.cl.modules.qrcode.task;

import cn.hutool.core.date.DateUtil;
import com.cl.entity.PersonCustomer;
import com.cl.modules.qrcode.entity.TableD1;
import com.cl.modules.qrcode.mapper.TableD1Mapper;
import com.cl.service.PersonCustomerService;
import com.cl.service.impl.WebSocket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 来访人员扫码推送 定时任务
 *
 * @author caolu
 * @date 2023/03/08
 */
@EnableScheduling
@Component
@Slf4j
public class CustomerTask {

    @Resource
    private TableD1Mapper tableD1Mapper;

    @Resource
    private PersonCustomerService personCustomerService;

    @Resource
    private WebSocket webSocket;
    public void customerPush() {
        log.info("================来访人员定时任务启动成功==========="+ DateUtil.date());
        int customerIndex = tableD1Mapper.customerIndex();
        if (customerIndex > 0) {
            List<TableD1> tableD1s = tableD1Mapper.listNew(customerIndex);
            ArrayList<PersonCustomer> list = new ArrayList<>();
            for (TableD1 item : tableD1s) {
                PersonCustomer customer = new PersonCustomer();
                customer.setName(item.getName());
                customer.setPhone(item.getPhone());
                customer.setAddress(item.getAddress());
                customer.setCarNum(item.getCarNum());
                customer.setRemark(item.getReason());
                customer.setHostname(item.getHostName());
                customer.setComingTime(item.getRecordTime());
                list.add(customer);
            }
            boolean saveBatch = personCustomerService.saveBatch(list);
            if(saveBatch){
                tableD1Mapper.resetIndex();
                webSocket.sendAllMessage("CustomerTask");
            }
        }
        log.info("================来访人员定时任务结束==============");
    }
}
