package com.cl.modules.qrcode.task;


import cn.hutool.core.date.DateUtil;
import com.cl.entity.ClockingInfo;
import com.cl.modules.qrcode.entity.TableD3;
import com.cl.modules.qrcode.mapper.TableD3Mapper;
import com.cl.service.ClockingInfoService;
import com.cl.service.impl.WebSocket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 住户打卡 定时任务
 *
 * @author caolu
 * @date 2023/03/08
 */
@EnableScheduling
@Component@Slf4j
public class PunchInTask {

    @Resource
    private TableD3Mapper tableD3Mapper;

    @Resource
    private ClockingInfoService clockingInfoService;

    @Resource
    private WebSocket webSocket;

    public void residentPunch(){
        log.info("================打卡信息定时任务启动成功==========="+ DateUtil.date());
        //查询触发器值
        int punchInIndex = tableD3Mapper.punchInIndex();
        if(punchInIndex>0){
            List<TableD3> tableD3s = tableD3Mapper.listNewPunch(punchInIndex);
            ArrayList<ClockingInfo> list = new ArrayList<>();
            for(TableD3 item: tableD3s){
                ClockingInfo info = new ClockingInfo();
                info.setPhone(item.getPhone());
                info.setLocaltion(item.getLocation());
                info.setName(item.getName());
                info.setCreateTime(item.getRecordTime());
                info.setOtherDescription(item.getOtherDescription());
                info.setTemperature(item.getTemperature());
                list.add(info);
            }
            boolean saveBatch = clockingInfoService.saveBatch(list);
            if (saveBatch){
                tableD3Mapper.resetPunch();
                webSocket.sendAllMessage("PunchInTask");
            }
        }
        log.info("================打卡信息定时任务结束==========="+ DateUtil.date());
    }

}
