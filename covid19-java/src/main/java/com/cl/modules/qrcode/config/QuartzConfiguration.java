package com.cl.modules.qrcode.config;

import cn.hutool.core.date.DateUtil;
import com.cl.modules.qrcode.task.CustomerTask;
import com.cl.modules.qrcode.task.PunchInTask;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

@Configuration
public class QuartzConfiguration {

    @Bean(name = "customerJobDetail")
    public MethodInvokingJobDetailFactoryBean customerJobDetail(CustomerTask customerTask){
        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
        //是否并发执行
        jobDetail.setConcurrent(false);
        //添加任务对象
        jobDetail.setTargetObject(customerTask);
        //添加需要执行的方法
        jobDetail.setTargetMethod("customerPush");
        return jobDetail;
    }

    @Bean(name = "customerTrigger")
    public SimpleTriggerFactoryBean customerTrigger(JobDetail customerJobDetail){
        SimpleTriggerFactoryBean trigger = new SimpleTriggerFactoryBean();
        trigger.setJobDetail(customerJobDetail);
        //设置任务自动延迟
        trigger.setStartDelay(0);
        //设置定时任务启动时间
        trigger.setStartTime(DateUtil.date());
        //设置定时任务执行间隔
        trigger.setRepeatInterval(30000);
        return trigger;
    }


    @Bean(name = "punchInJobDetail")
    public MethodInvokingJobDetailFactoryBean punchInJobDetail(PunchInTask punchInTask){
        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
        //是否并发执行
        jobDetail.setConcurrent(false);
        //添加任务对象
        jobDetail.setTargetObject(punchInTask);
        //添加需要执行的方法
        jobDetail.setTargetMethod("residentPunch");
        return jobDetail;
    }

    @Bean(name = "punchInTrigger")
    public SimpleTriggerFactoryBean punchInTrigger(JobDetail punchInJobDetail){
        SimpleTriggerFactoryBean trigger = new SimpleTriggerFactoryBean();
        trigger.setJobDetail(punchInJobDetail);
        //设置任务自动延迟
        trigger.setStartDelay(0);
        //设置定时任务启动时间
        trigger.setStartTime(DateUtil.date());
        //设置定时任务执行间隔
        trigger.setRepeatInterval(30000);
        return trigger;
    }


    //配置任务调度器
    @Bean(name = "scheduler")
    public SchedulerFactoryBean schedulerFactoryBean(Trigger customerTrigger,Trigger punchInTrigger){
        SchedulerFactoryBean factoryBean = new SchedulerFactoryBean();
        //延时启动
        factoryBean.setStartupDelay(1);
        //注册触发器
        factoryBean.setTriggers(customerTrigger,punchInTrigger);
        return factoryBean;
    }

}
