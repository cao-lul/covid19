/**
 * 转换时间格式
 * @param fmt
 * @param datestr
 * @returns {*}
 */
export function formatDate(fmt,datestr)
{
    var date = new Date(datestr);
    let ret;
        const opt = {
            "Y+": date.getFullYear().toString(),        // 年
            "m+": (date.getMonth() + 1).toString(),     // 月
            "d+": date.getDate().toString(),            // 日
            "H+": date.getHours().toString(),           // 时
            "M+": date.getMinutes().toString(),         // 分
            "S+": date.getSeconds().toString()          // 秒
            // 有其他格式化字符需求可以继续添加，必须转化成字符串
        };
        for (let k in opt) {
            ret = new RegExp("(" + k + ")").exec(fmt);
            if (ret) {
                fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
            };
        };
        return fmt;
}

// 把日期范围构造成beginTime和endTime
export function addDateRange(params,dateRange) {
  var search = params
  search.beginTime = ''
  search.endTime = ''
  if(dateRange != null){
    if (dateRange.length > 0) {
      search.beginTime = dateRange[0];
      search.endTime = dateRange[1];
    }
  }
  return search
}

/**
 * 时间戳转换为时间
 * @param time
 * @returns {string}
 */
export function changeTimeFormat(time) {
  var date = new Date(time);
  var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
  var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
  var hh = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
  var mm = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
  return date.getFullYear() + "-" + month + "-" + currentDate+" "+hh + ":" + mm;
  //返回格式：yyyy-MM-dd hh:mm
}

/**
 * 四舍五入保留2位小数（若第二位小数为0，则保留一位小数）
 * @param num
 * @returns {boolean|number}
 */
export function keepTwoDecimal(num) {
  var result = parseFloat(num);
  if (isNaN(result)) {
    alert('传递参数错误，请检查！');
    return false;
  }
  result = Math.round(num * 100) / 100;
  return result;
}
