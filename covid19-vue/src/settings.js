module.exports = {
  title: '社区疫情管理系统',

  /**
   * @type {boolean} true | false
   * @description 是否显示设置右侧面板
   */
  showSettings: false,

  /**
   * @type {boolean} true | false
   * @description 是否使用tagsView
   */
  tagsView: true,

  /**
   * @type {boolean} true | false
   * @description 是否固定标题
   */
  fixedHeader: true,

  /**
   * @type {boolean} true | false
   * @description 是否在边栏中显示徽标
   */
  sidebarLogo: true,

  /**
   * @type {string | array} 'production' | ['production', 'development']
   * @description Need show err logs component.
   * The default is only used in the production env
   * If you want to also use it in dev, you can pass ['production', 'development']
   */
  errorLog: 'production'
}
