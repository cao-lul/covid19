import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css'

import Element from 'element-ui'
import './styles/element-variables.scss'
// import enLang from 'element-ui/lib/locale/lang/en'

import * as echarts from "echarts";

Vue.prototype.$echarts = echarts;

import '@/styles/index.scss' // global css
import { scrollY } from "@/utils/scorllbar.js"; //滚动条
Vue.prototype.escrollY = scrollY     //滚动条
import App from './App'
import store from './store'
import router from './router'
// import directive from './directive' // directive
import plugins from './plugins' // plugins

import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

// import * as filters from './filters' // global filters

// 导入封装信息确认提示框组件脚本
import myconfirm from '@/utils/myconfirm'
// 导入清空表单工具
import resetForm from '@/utils/resetForm'
// 导入表单回显脚本
import objCopy from '@/utils/objCopy'
//全局拖拽引入
import '@/utils/directive.js'

// 全局方法挂载
Vue.prototype.$myconfirm = myconfirm;
Vue.prototype.$resetForm = resetForm;
Vue.prototype.$objCopy = objCopy;

// 全局组件挂载
// Vue.use(directive)
Vue.use(plugins)

//导入按钮权限判断
// import hasPermission from '@/permission/index'
// Vue.prototype.hasPermission = hasPermission;

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

Vue.use(Element, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  // locale: enLang // 如果使用中文，无需设置，请删除
})

// register global utility filters
// Object.keys(filters).forEach(key => {
//   Vue.filter(key, filters[key])
// })

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
