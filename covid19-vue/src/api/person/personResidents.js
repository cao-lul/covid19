import http from '@/utils/request';

export default {
    //查询部门列表
    async personResidentsPage(param) {
        return await http.post("/api/person_residents/page", param);
    },

    async personResidentsSave(param) {
        return await http.post("/api/person_residents/save", param);
    },

    async personResidentsDel(param) {
        return await http.post("/api/person_residents/delete", param);
    },

    async personResidentsDelBatch(param) {
        return await http.post("/api/person_residents/deleteBatch", param);
    },

    async getPersonResidents(param) {
        return await http.post("/api/person_residents/detail", param);
    },
}