import http from '@/utils/request';

export default {
    //查询部门列表
    async personCustomerPage(param) {
        return await http.post("/api/person_customer/page", param);
    },

    async personCustomerSave(param) {
        return await http.post("/api/person_customer/save", param);
    },

    async personCustomerDel(param) {
        return await http.post("/api/person_customer/delete", param);
    },

    async personCustomerDelBatch(param) {
        return await http.post("/api/person_customer/deleteBatch", param);
    },

    async getPersonCustomer(param) {
        return await http.post("/api/person_customer/detail", param);
    },
}