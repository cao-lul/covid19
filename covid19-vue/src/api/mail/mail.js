import http from '@/utils/request'

export default {
  //邮件发送
  async sendMail(param) {
    return await http.post("/api/sys-mail/send", param);
  },
  async getMail(param) {
    return await http.get("/api/sys-mail/getMail", param);
  },

  async attachment(param) {
    return await http.post("/api/sys-mail/attachment", param);
  },

  async history(param) {
    return await http.post("/api/sys-mail/history", param)
  },
}
