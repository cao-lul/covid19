import http from '@/utils/request';

export default {
  //查询菜单列表
  async getMenuList(param) {
    return await http.get("/system/menu/list", param);
  },

  //获取所属菜单列表
  async getParentTreeList() {
    return await http.get("/system/menu/parent/list");
  },

  //添加菜单
  async addMenu(param) {
    return await http.post("/system/menu/add", param);
  },

  //修改菜单
  async updateMenu(param) {
    return await http.put("/system/menu/update", param);
  },

  //查询某个菜单下是否存在子菜单
  async getCheckMenu(menuId) {
    return await http.getRestApi("/system/menu/check", menuId);
  },

  //删除菜单
  async deleteMenu(menuId) {
    return await http.delete("/system/menu/delete", menuId);
  }
}
