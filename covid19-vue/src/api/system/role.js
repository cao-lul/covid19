import http from '@/utils/request';

export default {
  //分页查询角色列表
  async getRolePage(param) {
    return await http.post("/system/role/page", param);
  },

  //添加角色
  async addRole(param) {
    return await http.post("/system/role/add", param);
  },

  //修改角色
  async updateRole(param) {
    return await http.put("/system/role/update", param);
  },

  //检查角色是否被分配出去
  async getCheckRole(roleId) {
    return await http.getRestApi("/system/role/getCheckRole", roleId);
  },

  //删除角色
  async deleteRole(roleId) {
    return await http.delete("/system/role/delete", roleId);
  },
  
  //分配权限-查询权限树数据
  async getAssignMenuTree(roleId) {
    return await http.getRestApi("/system/menu/getAssignMenuTree", roleId);
  },

  //分配权限-保存权限数据
  async saveRoleAssign(param) {
    return await http.post("/system/role/saveRoleAssign", param);
  },
}
