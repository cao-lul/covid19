import http from '@/utils/request';

export default {
  //分页查询用户列表
  async getUserPage(param) {
    return await http.post("/system/user/page", param);
  },

  //添加用户
  async addUser(param) {
    return await http.post("/system/user/add", param);
  },

  //修改用户
  async updateUser(param) {
    return await http.put("/system/user/update", param);
  },

  //删除用户
  async deleteUser(userId) {
    return await http.delete("/system/user/delete", userId);
  },

  //获取分配角色列表数据
  async getRoleIdByUserId(params) {
    return await http.getRestApi("/system/user/getRoleByUserId", params);
  },

  //用户分配角色
  async assignRoleSave(params) {
    return await http.post("/system/user/saveUserRole", params)
  }
}
