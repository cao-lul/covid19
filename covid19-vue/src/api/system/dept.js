import http from '@/utils/request';

export default {
  //查询部门列表
  async getDeptList(param) {
    return await http.get("/system/dept/list", param);
  },

  //获取所属部门列表
  async getParentTreeList() {
    return await http.get("/system/dept/parent/list");
  },

  //添加部门
  async addDept(param) {
    return await http.post("/system/dept/add", param);
  },

  //修改部门
  async updateDept(param) {
    return await http.put("/system/dept/update", param);
  },

  //查询某个部门下是否存在子部门
  async getCheckDept(deptId) {
    return await http.getRestApi("/system/dept/check", deptId);
  },

  //删除部门
  async deleteDept(deptId) {
    return await http.delete("/system/dept/delete", deptId);
  }
}
