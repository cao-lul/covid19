import http from '@/utils/request';

export default {
  //分页查询用户列表
  async getVaccinationPage(param) {
    return await http.post("/api/vaccinationRecord/page", param);
  },

  async save(param) {
    return await http.post("/api/vaccinationRecord/save", param);
  },
}
