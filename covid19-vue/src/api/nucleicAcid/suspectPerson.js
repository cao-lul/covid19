import http from '@/utils/request';

export default {
  //分页查询用户列表
  async page(param) {
    return await http.post("/api/suspectedPerson/page", param);
  },

  async save(param) {
    return await http.post("/api/suspectedPerson/save", param);
  },

  async delById(param) {
    return await http.post("/api/suspectedPerson/delete", param);
  },

  async delBatch(param) {
    return await http.post("/api/suspectedPerson/deleteBatch", param);
  },

  async getById(param) {
    return await http.post("/api/suspectedPerson/detail", param);
  },


}
