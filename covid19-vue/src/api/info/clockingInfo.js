import http from '@/utils/request'

export default {
  //邮件发送
  async page(param) {
    return await http.post("/api/clockingInfo/page", param);
  },
  async report(param) {
    return await http.post("/api/clockingInfo/report", param);
  },

}
