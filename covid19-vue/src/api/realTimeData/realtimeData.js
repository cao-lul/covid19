import http from '@/utils/request';

export default {
    //查询部门列表
    async epidemicHotspot(param) {
        return await http.get("/api/covid19/epidemicHotspot", param);
    },
}