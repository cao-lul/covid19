import http from '@/utils/request';

export default {
    //查询部门列表
    async getPurchaseRecord(param) {
      return await http.post("/api/covid_purchase_record/page", param);
    },

    async savePurchase(param) {
      return await http.post("/api/covid_purchase_record/save", param);
    },

    async updatePurchase(param) {
      return await http.post("/api/covid_purchase_record/save", param);
    },

    async purchaseDetail(param) {
      return await http.post("/api/covid_purchase_record/detail", param);
    },

    async delete(param) {
      return await http.post("/api/covid_purchase_record/delete", param);
    },

    async deleteBatch(param) {
      return await http.post("/api/covid_purchase_record/deleteBatch", param);
    },
   
    //爬取数据测试
    async realTimeData(param) {
      return await http.get("/api/covid19/realTimeData", param);
    },
}