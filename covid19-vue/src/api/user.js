import http from '@/utils/request'

/**
* 用户登录
* @returns
*/
export async function login(data) {
  return await http.login("/api/user/login", data)
}

/**
* 获取用户信息和权限信息
* @returns
*/
export async function getInfo() {
  return await http.get("/api/sysUser/getInfo")
}

/**
* 退出登录
* @returns
*/
export async function logout(param) {
  return await http.post("/api/sysUser/logout", param);
}

/**
* 获取用户菜单数据
*/
export async function getMenuList() {
  return await http.get("/api/sysUser/getMenuList");
}

/**
* 刷新token
* @returns
*/
export async function refreshTokenApi(params) {
  return await http.post("/system/user/refreshToken", params);
}

// 注意：此处调用的是mock数据，并非真实的后端接口数据
// mock数据在工程的 mock/user.js里面，此处的token是写死的
// export function login(data) {
//   return request({
//     url: '/vue-element-admin/user/login',
//     method: 'post',
//     data
//   })
// }

// export function getInfo(token) {
//   return request({
//     url: '/vue-element-admin/user/info',
//     method: 'get',
//     params: { token }
//   })
// }

// export function logout() {
//   return request({
//     url: '/vue-element-admin/user/logout',
//     method: 'post'
//   })
// }
